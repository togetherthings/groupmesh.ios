//
//  ViewController.swift
//  MyLocationMap
//
//  Created by Tyre King on 1/3/16.
//  Copyright © 2016 Tyre King. All rights reserved.
//

//need JONATHAN"S HELP TO TRANSLATE THIS CODE FROM OBJECTIVE C TO SWIFT 
//http://www.appcoda.com/ios-programming-101-drop-a-pin-on-map-with-mapkit-api/
// GO TO ZOOM INTO THE CURRENT LOCATION



import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    var location = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        location.delegate = self
        location.desiredAccuracy = kCLLocationAccuracyBest
        location.requestWhenInUseAuthorization()
        location.startUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

