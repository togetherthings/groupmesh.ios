//
//  CellOutline.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 3/18/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

class PCVCOutline: UIView {
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        let rectanglePath = UIBezierPath(roundedRect: rect, cornerRadius: 4)
        
        // Set path border
        //let color = UIColor(red: 108.0/255.0, green: 151.0/255.0, blue: 202.0/255.0, alpha: 1.0)
        let color = UIColor.clearColor()
        let borderLayer = CAShapeLayer()
        borderLayer.path = rectanglePath.CGPath
        borderLayer.fillColor = color.CGColor
        borderLayer.strokeColor = color.CGColor
        borderLayer.lineWidth = 1
        borderLayer.frame = rectanglePath.bounds
        self.layer.addSublayer(borderLayer)
        
        color.setFill()
        rectanglePath.fill()
    }
}
