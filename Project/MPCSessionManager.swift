//
//  MPCSessionManager.swift
//  ChatBox
//
//  Created by Jonathan Noyola on 5/18/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import MultipeerConnectivity

protocol MPCSessionManagerDelegate : NSObjectProtocol {

    // Received data from remote peer.
    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID)
    
    // Received a byte stream from remote peer.
    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID)
    
    // Start receiving a resource from remote peer.
    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress)
    
    // Finished receiving a resource from remote peer and saved the content
    // in a temporary location - the app is responsible for moving the file
    // to a permanent location within its sandbox.
    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?)
}

class MPCSessionManager: NSObject, MCSessionDelegate {

    var delegate: MPCSessionManagerDelegate?

    var myPeer: MCPeerID!
    
    var openSessionToSize = [MCSession: Int]()

    var peerIDToSession = [String: MCSession]()
    
    init(myPeer: MCPeerID) {
        super.init()
    
        self.myPeer = myPeer
    }
    
    func chooseAndIncrementSession() -> MCSession {
        var session: MCSession!
        if openSessionToSize.isEmpty {
        
            // Create a new session
            session = MCSession(peer: myPeer)
            session.delegate = self
            openSessionToSize[session] = 1
            
        } else {
        
            // Choose an existing session
            let pair = openSessionToSize.first!
            session = pair.0
            let prevSize = pair.1
            if prevSize >= 6 {
            
                // If the session is now full, remove it from openSessions
                openSessionToSize.removeValueForKey(session)
                
            } else {
            
                // Otherwise update its size
                openSessionToSize[session] = prevSize + 1
                
            }
        }
        
        return session
    }
    
    func invitePeer(peer: MCPeerID, withBrowser mpcBrowser: MCNearbyServiceBrowser) {
        let session = chooseAndIncrementSession()
        mpcBrowser.invitePeer(peer, toSession: session, withContext: nil, timeout: 20)
    }

    func peerConnectedToSession(session: MCSession, peer: MCPeerID) {
        peerIDToSession[peer.displayName] = session
    }
    
    func peerDisconnectedFromSession(session: MCSession, peer: MCPeerID) {
        peerIDToSession.removeValueForKey(peer.displayName)
        
        var prevSize = openSessionToSize[session]
        if prevSize == nil {
            prevSize = 7
        }
        
        if prevSize <= 1 {
        
            // If the session is now empty, disconnect and remove
            session.disconnect()
            session.delegate = nil
            openSessionToSize.removeValueForKey(session)
            
        } else {
        
            // Otherwise update its size
            openSessionToSize[session] = prevSize! - 1
        
        }
    }
    
    /////////////////////////////
    // MARK: MCSessionDelegate //
    /////////////////////////////
    
    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState) {
        switch state {
            case MCSessionState.Connected:
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.peerConnectedToSession(session, peer: peerID)
                })
            
            case MCSessionState.Connecting:
                break
            
            default:
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.peerDisconnectedFromSession(session, peer: peerID)
                })
        }
    }

    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID) {
        delegate?.session(session, didReceiveData: data, fromPeer: peerID)
    }

    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        delegate?.session(session, didReceiveStream: stream, withName: streamName, fromPeer: peerID)
    }

    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress) {
        delegate?.session(session, didStartReceivingResourceWithName: resourceName, fromPeer: peerID, withProgress: progress)
    }

    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?) {
        delegate?.session(session, didFinishReceivingResourceWithName: resourceName, fromPeer: peerID, atURL: localURL, withError: error)
    }

    // func session(session: MCSession, didReceiveCertificate certificate: [AnyObject]?, fromPeer peerID: MCPeerID, certificateHandler: (Bool) -> Void)
}
