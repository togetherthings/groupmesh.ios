//
//  CollectionViewCell.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 3/6/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

protocol ContentAwareCollectionViewCell {
    func configure(model model: AnyObject, prototype: Bool)
    func fittedSizeForConstrainedSize(constrainedSize: CGSize) -> CGSize
}

class CollectionViewCell: UICollectionViewCell, ContentAwareCollectionViewCell {
    
    struct Const {
        static var ReuseIdentifier = "CollectionViewCell"
    }
    
    let TextLabelSidePadding: CGFloat = 8.0
    
    let textLabel = UILabel()
    let imageView = UIImageView()
    let containerView = PCVCOutline()
    
    var imageViewHeight: CGFloat = 0
    
    convenience init() {
        self.init(frame: CGRect.zero)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        contentView.addSubview(containerView)
        contentView.backgroundColor = UIColor.whiteColor()
        containerView.backgroundColor = UIColor.whiteColor()
        textLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 14)
        textLabel.textColor = UIColor.blackColor()
        textLabel.numberOfLines = 0
        textLabel.lineBreakMode = .ByWordWrapping
        textLabel.textAlignment = .Center
        containerView.addSubview(textLabel)
        
        // Set Image
        imageView.frame = CGRectMake(21, containerView.bounds.height / 2, 50, 50)
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
        imageView.contentMode = .ScaleAspectFill
        imageView.layer.cornerRadius = 28 - 3.14
    }
    
    func configure(model model: AnyObject, prototype: Bool) {
        let info = model as! Data
        textLabel.text = info.text
        if let data = info.data, let image = data["image"] {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.imageView.image = image as? UIImage
                self.containerView.addSubview(self.imageView)
            })
            let fullName = textLabel.text!.componentsSeparatedByString(" ")
            let firstName = fullName[0]
            textLabel.text = firstName
            imageViewHeight = imageView.frame.height + 50
        }
        containerView.setNeedsDisplay()
    }
    
    func fittedSizeForConstrainedSize(size: CGSize) -> CGSize {
        let textLabelConstrainedSize = CGSize(width: size.width - TextLabelSidePadding * 2.0, height: size.height - TextLabelSidePadding * 2.0)
        let textLabelSize = textLabel.sizeThatFits(textLabelConstrainedSize)
        
        return CGSize(width: size.width, height: textLabelSize.height + (TextLabelSidePadding * 2.0) + imageViewHeight)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.frame = contentView.bounds
        let textLabelFrame = CGRectInset(containerView.bounds, TextLabelSidePadding, TextLabelSidePadding)
        textLabel.frame = textLabelFrame
    }
    
}
