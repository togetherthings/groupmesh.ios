//
//  CMTextView.swift
//  GroupMesh
//
//  Created by Leo Da Costa on 7/25/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit

class CMTextView: UITextView {
    
    private var originalText: String!
    
    override var text: String! {
        didSet {
            originalText = text
            originalAttributedText = nil
            if needsTrim() { updateText() }
        }
    }
    
    private var originalAttributedText: NSAttributedString!
    
    override var attributedText: NSAttributedString! {
        didSet {
            originalAttributedText = attributedText
            originalText = nil
            if needsTrim() { updateText() }
        }
    }
    
    var maximumNumberOfLines: Int = 0 {
        didSet { setNeedsLayout() }
    }
    
    var trimText: NSString? {
        didSet { setNeedsLayout() }
    }
    
    var shouldTrim: Bool = false {
        didSet { setNeedsLayout() }
    }
    
    
    ////////////////////////////////////////////////////////////////////
    // MARK: - Initialization
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setup()
    }
    
    convenience init(frame: CGRect) {
        self.init(frame: frame, textContainer: nil)
    }
    
    convenience init() {
        self.init(frame: CGRectZero, textContainer: nil)
    }
    
    convenience init(maximumNumberOfLines: Int, trimText: String?, shouldTrim: Bool) {
        self.init()
        self.maximumNumberOfLines = maximumNumberOfLines
        self.trimText = trimText
        self.shouldTrim = shouldTrim
    }
    
    //
    ////////////////////////////////////////////////////////////////////
    
    override func layoutSubviews() {
        super.layoutSubviews()
        needsTrim() ? updateText() : resetText()
    }
    
    private func setup() {
        self.tag = DataTag.Text.rawValue
        self.font = UIFont.systemFontOfSize(15)
        self.layer.borderWidth = 1
        self.layer.cornerRadius = CMConstant.cornerRadius
        self.layer.borderColor = CMColor.lightGray.CGColor
    }
    
    func readOnly() {
        self.editable = false
        self.scrollEnabled = false
        self.userInteractionEnabled = false
    }
    
    func needsTrim() -> Bool {
        return shouldTrim && trimText != nil
    }
    
    func updateText() {
        //update text view
    }
    
    func resetText() {  
        //reset text view
    }
    
    func textLength() -> CGFloat {
        return CGFloat((self.textContainer.layoutManager?.characterRangeThatFits(self.textContainer).length)!)
    }
}

extension NSLayoutManager {
    
    func characterRangeThatFits(textContainer: NSTextContainer) -> NSRange {
        var rangeThatFits = self.glyphRangeForTextContainer(textContainer)
        rangeThatFits = self.characterRangeForGlyphRange(rangeThatFits, actualGlyphRange: nil)
        return rangeThatFits
    }
    
    func numberOfLinesThatFits(textContainer: NSTextContainer) -> Int {
        let layoutManager = textContainer.layoutManager
        let numberOfGlyphs = (layoutManager?.numberOfGlyphs)!
        
        var numberOfLines: Int = 0
        var lineRange = NSRange()
        
        for (j, i) in 0.stride(to: numberOfGlyphs, by: NSMaxRange(lineRange)).enumerate() {
            layoutManager?.lineFragmentRectForGlyphAtIndex(i, effectiveRange: &lineRange)
            numberOfLines += j
        }
        
        return numberOfLines
    }
    
    func numberOfLines(textView: UITextView) -> CGSize {
        let string = textView.text
        let style = NSMutableParagraphStyle()
        style.lineBreakMode = NSLineBreakMode.ByWordWrapping
        let size = string.sizeWithAttributes([NSFontAttributeName: textView.font!, NSParagraphStyleAttributeName: style])
        
        return size
    }
}

extension String {
    func sizeForWidth(width: CGFloat, font: UIFont) -> CGSize {
        let attr = [NSFontAttributeName: font]
        let height = NSString(string: self).boundingRectWithSize(CGSize(width: width, height: CGFloat.max), options:.UsesLineFragmentOrigin, attributes: attr, context: nil).height
        return CGSize(width: width, height: ceil(height))
    }
}
