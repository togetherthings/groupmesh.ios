//
//  ProfileView.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 2/26/16.
//  Copyright © 2016 Chatbox. All rights reserved.
//

import UIKit

class ProfileView: UIViewController, UIScrollViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    struct Properties {
        var organizations: [String]!
        var interests: [String]!
        var cells = [ProfileTableCell]()
        
        let defaultCellHeight: CGFloat = 309
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var headerName: UILabel!
    
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var pullDownView: UIView!
    var properties = Properties()
    var started = true
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let imagePicker = UIImagePickerController()
    let offset_HeaderStop:CGFloat = 30.0 // At this offset the Header stops its transformations
    let distance_W_LabelHeader:CGFloat = 60.0 // The distance between bottom of header and header label
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.hidden = false
        self.navigationController!.navigationBar.barStyle = .Black
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set image UI
        imagePicker.delegate = self
        imageView.layer.cornerRadius =  (imageView.frame.height / 2)
        imageView.contentMode = .ScaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
        imageView.image = user.profileImage
        
        header.clipsToBounds = true
        
        // Navigation Bar Setting
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        self.navigationController!.view.backgroundColor = UIColor.clearColor()
        
        // Set Profile Info
        let data: [String: AnyObject] = ["name" : [user.firstName, user.lastName], "organizations" : user.organizations, "interests" : user.interests]
        setProfile(data)
        
        configureTableView()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileView.didChangeContentSize(_:)), name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Set Profile
    
    func setProfile(data: [String : AnyObject]) {
        var modified = false
        let keys = Array(data.keys)
        for key in keys {
            switch(key) {
            case "name":
                if let name = data["name"] as? [String] {
                    nameLabel.text = name[0] + " " + name[1]
                    headerName.text = name[0] + " " + name[1]
                }
            case "organizations":
                if let organizations = data["organizations"] as? [String] {
                    properties.organizations = organizations
                    modified = true
                }
            case "interests":
                if let interests = data["interests"] as? [String] {
                    properties.interests = interests
                    modified = true
                }
            case "image":
                if let array = data["image"] as? [AnyObject] {
                    imageView.image = array[0] as? UIImage
                }
            default: break
            }
        }
        if modified == true { tableView.reloadData() }
    }
    
    func getList(array: [String]) -> String {
        var list = ""
        for i in 0 ..< array.count {
            let item = array[i] 
            if i == array.count - 1 { list += item }
            else { list += "\(item), " }
        }
        return list
    }
    
    // Upload Image
    
    @IBAction func uploadPictureAction(sender: AnyObject) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    
    // Mark: - UIImagePickerControllerDelegate methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let resizedImage = ImageManager.resizeImage(pickedImage, toTheSize: CGSizeMake(120, 120))
            imageView.image = resizedImage
            user.save(["image" : [resizedImage, false]])
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: - Table View
    
    func configureTableView() {
        tableView.estimatedRowHeight = 1500.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()
    }
    
    func didChangeContentSize(notification: NSNotification) {
        let data = notification.object as! [String : AnyObject]
        let cell = data["cell"] as! Int
        let height = data["height"] as! CGFloat
        let cellHeight = properties.cells[cell].height
        if cellHeight != height {
            tableView.reloadData()
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if properties.cells.count > 0 {
            let cell = properties.cells[indexPath.row]
            let size = cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
            var newHeight = size.height + cell.height
            if cell.height < properties.defaultCellHeight {
                newHeight = size.height + properties.defaultCellHeight
            }
            return newHeight
        }
        else { return 300 }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PTCell")! as! ProfileTableCell
        if indexPath.row == 0 {
            cell.makeTitle("Organizations")
            cell.instantiateData(properties.organizations)
            if properties.cells.count < 2 { properties.cells.append(cell) }
        }
        else if indexPath.row == 1 {
            cell.makeTitle("Interests")
            cell.instantiateData(properties.interests)
            if properties.cells.count < 2 { properties.cells.append(cell) }
        }
        return cell
    }
    
    
    // MARK: - Profile UI
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        
        var avatarTransform = CATransform3DIdentity
        var headerTransform = CATransform3DIdentity
        
        // PULL DOWN
        
        if offset < 0 {
            
            let headerScaleFactor:CGFloat = -(offset) / header.bounds.height
            let headerSizevariation = ((header.bounds.height * (1.0 + headerScaleFactor)) - header.bounds.height) / 2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            
            header.layer.transform = headerTransform
            header.layer.zPosition = 0
            headerName.hidden = true
        }
            
            // SCROLL UP/DOWN
            
        else {
            
            // Header
            
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
            
            // Header Items
            
            headerName.hidden = false
            let alignToNameLabel = -offset + nameLabel.frame.origin.y + header.frame.height - (offset_HeaderStop * 3)
            
            headerName.frame.origin = CGPointMake(headerName.frame.origin.x, max(alignToNameLabel, distance_W_LabelHeader + offset_HeaderStop))
            
            // Avatar
            
            let avatarScaleFactor = (min(offset_HeaderStop, offset)) / imageView.bounds.height / 0.6
            let avatarSizeVariation = ((imageView.bounds.height * (1.0 + avatarScaleFactor)) - imageView.bounds.height) / 2.0
            avatarTransform = CATransform3DTranslate(avatarTransform, 0, avatarSizeVariation, 0)
            avatarTransform = CATransform3DScale(avatarTransform, 1.0 - avatarScaleFactor, 1.0 - avatarScaleFactor, 0)
            
            if offset <= offset_HeaderStop {
                
                if imageView.layer.zPosition < header.layer.zPosition{
                    header.layer.zPosition = 0
                }
                
            }
            else {
                if imageView.layer.zPosition >= header.layer.zPosition{
                    header.layer.zPosition = 2
                }
            }
        }
        
        // Apply Transformations
        
        header.layer.transform = headerTransform
        imageView.layer.transform = avatarTransform
    }
}

