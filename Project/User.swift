//
//  User.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 1/29/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import Foundation
import CoreData
import MultipeerConnectivity
import Photos

class User: NSObject, NSCoding {
    
    struct Information {
        var peerID: MCPeerID!
        var facebookID: String!
        var context: AnyObject!
        var isAdvertising: Bool!
        
        // Credentials
        var deviceID: NSString!
        var password: NSString!
        var token: NSString!
        
        var middleName: String!
        var name_format: String!
        var birthday: String!
        var gender: String!
        var hometown: String!
        
        var political: String!
        var religion: String!
        var profession: String!
        var major: String!
        var website: String!
        
        var interested_in: [String]!
        var highschool: [String]!
        var college: [String]!
        var languages: [String]!
        
        var location: Dictionary<String, AnyObject>!
        
        // Media
        var imageAssetCollection = PHAssetCollection()
        var videoAssetCollection = PHAssetCollection()
        
        init() {
            self.peerID = MCPeerID()
            self.facebookID = String()
            self.context = []
            self.isAdvertising = false
            self.deviceID = NSUUID().UUIDString
            self.password = NSString()
            self.token = NSString()
            self.middleName = String()
            self.name_format = String()
            self.birthday = String()
            self.gender = String()
            self.hometown = String()
            self.political = String()
            self.religion = String()
            self.profession = String()
            self.major = String()
            self.website = String()
            self.interested_in = [String]()
            self.highschool = [String]()
            self.college = [String]()
            self.languages = [String]()
            self.location = Dictionary<String, AnyObject>()
        }
    }
    
    var hidden: Information!
    
    var profileImage: UIImage!
    var adImage: UIImage!
    var hasDefaultImage: Bool!
    
    var firstName: String!
    var lastName: String!
    var email: String!
    
    var interests: [String]!
    var organizations: [String]!

    
    func getFullName() -> String {
        return firstName + " " + lastName
    }
    
    override init() {
        super.init()
        self.profileImage = UIImage()
        self.adImage = UIImage()
        self.hasDefaultImage = true
        self.firstName = String()
        self.lastName = String()
        self.email = String()
        self.interests = [String]()
        self.organizations = [String]()
        self.hidden = Information()
    }
    
    static func loadUser() {
        let archivedPublicUserData = person[0].valueForKey("public") as! NSData
        let archivedHiddenUserData = person[0].valueForKey("hidden") as! NSData
        
        let unarchivedPublicUserData = NSKeyedUnarchiver.unarchiveObjectWithData(archivedPublicUserData)
        let unarchivedHiddenUserData = NSKeyedUnarchiver.unarchiveObjectWithData(archivedHiddenUserData) as! NSDictionary
        
        user = unarchivedPublicUserData as! User
        user.hidden = Information(propertyListRepresentation: unarchivedHiddenUserData)
        
        // Set Profile
        /*let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let root = appDelegate.window?.rootViewController as! UITabBarController
        let navigator = root.viewControllers![2] as! UINavigationController
        let profileView = navigator.viewControllers[0] as! ProfileViewController*/
    }
    
    func save(data: [String : AnyObject]?) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "User")
        do {
            // Set/Create User
            let results = try managedContext.executeFetchRequest(fetchRequest)
            let usr: NSManagedObject!
            let main = results as! [NSManagedObject]
            if main.count == 0 {
                let entity = NSEntityDescription.entityForName("User", inManagedObjectContext: managedContext)
                usr = NSManagedObject(entity : entity!, insertIntoManagedObjectContext: managedContext)
            }
            else { usr = main[0] }
            
            if let _ = data {
                setManagedData(usr, data: data!)
            }
            
            let mappedHiddenUserData = user.hidden.propertyListRepresentation()
            let archivedHiddenUserData = NSKeyedArchiver.archivedDataWithRootObject(mappedHiddenUserData)
            let archivedPublicUserData = NSKeyedArchiver.archivedDataWithRootObject(user)

            usr.setValue(archivedHiddenUserData, forKey: "hidden")
            usr.setValue(archivedPublicUserData, forKey: "public")
            
            do {
                person = [usr]
                try managedContext.save()
                print("User Saved")
            } catch let error as NSError {
                print("\(error), \(error.userInfo)")
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    private func setManagedData(managedUser: NSManagedObject, data: [String : AnyObject]) {
        let keys = Array(data.keys)
        for key in keys {
            switch(key) {
            case "name":
                if let name = data["name"] as? [String] {
                    firstName = name[0]
                    lastName = name[1]
                    if hasDefaultImage == true {
                        profileImage = DefaultImage.makeUserDefaultImage([firstName, lastName])
                    }
                }
            case "email":
                if let email = data["email"] as? String {
                    self.email = email
                }
            case "organizations":
                if let organizations = data["organizations"] as? [String] {
                    self.organizations = organizations
                }
            case "interests":
                if let interests = data["interests"] as? [String] {
                    self.interests = interests
                }
            case "image":
                if let array = data["image"] as? [AnyObject] {
                    let image = array[0] as! UIImage
                    profileImage = ImageManager.resizeImage(image, toTheSize: CGSizeMake(120, 120))
                    adImage = ImageManager.resizeImage(image, toTheSize: CGSizeMake(50, 50))
                    hasDefaultImage = array[1] as! Bool
                }
            default: break
            }
        }
        if let userType = data["usertype"] as? String {
            if userType == "new" {
                newUser()
            }
        }
    }
    
    private func newUser() {
        // Set PeerID
        hidden.peerID = MCPeerID(displayName: user.getFullName())
        hidden.peerID.displayImage = adImage
        
        // Set Password
        hidden.password = NSString(string: Generator.randomAlphaNumericString(16))
        
        // Connect to server
        HTTPSServerAccess.register()
    }
    
    
    // MARK: - Coder / Decoder
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        guard let profileImageData = aDecoder.decodeObjectForKey("profileImage") as? NSData,
              let adImageData = aDecoder.decodeObjectForKey("adImage") as? NSData,
              let hasDefaultImage = Optional(aDecoder.decodeBoolForKey("hasDefaultImage")),
              let firstName = aDecoder.decodeObjectForKey("firstName") as? String,
              let lastName = aDecoder.decodeObjectForKey("lastName") as? String,
              let email = aDecoder.decodeObjectForKey("email") as? String,
              let interests = aDecoder.decodeObjectForKey("interests") as? [String],
              let organizations = aDecoder.decodeObjectForKey("organizations") as? [String]
            else { return nil }
        
        let decodedAdImage = UIImage(data: adImageData)
        let decodedProfileImage = UIImage(data: profileImageData)
        
        self.profileImage = decodedProfileImage
        self.adImage = decodedAdImage
        self.hasDefaultImage = hasDefaultImage
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.interests = interests
        self.organizations = organizations
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        let adImageData = UIImageJPEGRepresentation(adImage, 1)
        let profileImageData = UIImageJPEGRepresentation(profileImage, 1)
        
        aCoder.encodeObject(profileImageData, forKey: "profileImage")
        aCoder.encodeObject(adImageData, forKey: "adImage")
        aCoder.encodeBool(hasDefaultImage, forKey: "hasDefaultImage")
        aCoder.encodeObject(firstName, forKey: "firstName")
        aCoder.encodeObject(lastName, forKey: "lastName")
        aCoder.encodeObject(email, forKey: "email")
        aCoder.encodeObject(interests, forKey: "interests")
        aCoder.encodeObject(organizations, forKey: "organizations")
    }
}

extension User.Information: PropertyListReadable {
    
    init?(propertyListRepresentation: NSDictionary?) {
        guard let values = propertyListRepresentation else { return nil }
        if let peerID = values["peerID"] as? MCPeerID,
               facebookID = values["facebookID"] as? String,
               context = values["context"],
               isAdvertising = values["isAdvertising"] as? Bool,
        
               // Credentials
               deviceID = values["deviceID"] as? NSString,
               password = values["password"] as? NSString,
               token = values["token"] as? NSString,
        
               // General
               middleName = values["middleName"] as? String,
               name_format = values["name_format"] as? String,
               birthday = values["birthday"] as? String,
               gender = values["gender"] as? String,
               hometown =  values["hometown"] as? String,
               political = values["political"] as? String,
               religion = values["religion"] as? String,
               profession = values["profession"] as? String,
               major = values["major"] as? String,
               website = values["website"] as? String,
               interested_in = values["interested_in"] as? [String],
               highschool = values["highschool"] as? [String],
               college = values["college"] as? [String],
               languages = values["languages"] as? [String],
               location = values["location"] as? Dictionary<String, AnyObject> {
            
            // Map
            self.peerID = peerID
            self.facebookID = facebookID
            self.context = context
            self.isAdvertising = isAdvertising
            self.deviceID = deviceID
            self.password = password
            self.token = token
            self.middleName = middleName
            self.name_format = name_format
            self.birthday = birthday
            self.gender = gender
            self.hometown = hometown
            self.political = political
            self.religion = religion
            self.profession = profession
            self.major = major
            self.website = website
            self.interested_in = interested_in
            self.highschool = highschool
            self.college = college
            self.languages = languages
            self.location = location
        }
    }
    
    func propertyListRepresentation() -> NSDictionary {
        let representation: [String : AnyObject] = [
            "peerID" : self.peerID,
            "facebookID" : self.facebookID,
            "context" : self.context,
            "isAdvertising" : self.isAdvertising,
            
            // Credentials
            "deviceID" : self.deviceID,
            "password" : self.password,
            "token" : self.token,
            
            // General
            "middleName" : self.middleName,
            "name_format" : self.name_format,
            "birthday" : self.birthday,
            "gender" : self.gender,
            "hometown" : self.hometown,
            "political" : self.political,
            "religion" : self.religion,
            "profession" : self.profession,
            "major" : self.major,
            "website" : self.website,
            "interested_in" : self.interested_in,
            "highschool" : self.highschool,
            "college" : self.college,
            "languages" : self.languages,
            "location" : self.location
        ]
        
        return representation
    }
}

