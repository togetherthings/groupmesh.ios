//
//  MPCGraphBrowser.swift
//  ChatBox
//
//  Created by Jonathan Noyola on 5/18/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import MultipeerConnectivity

class MPCGraphBrowser: NSObject, MCNearbyServiceBrowserDelegate {

    var myPeer: MCPeerID!
    
    var mpcGraph: MPCGraph!
    
    var mpcSessionManager: MPCSessionManager!

    var mpcBrowser: MCNearbyServiceBrowser!
    
    var foundPeerIDToGraphID = [String: String]()
    
    var foundGraphIDToPeers = [String: Set<MCPeerID>]()
    
    var peerCollectionTimer: NSTimer?
    
    init(myPeer: MCPeerID, mpcGraph: MPCGraph, mpcSessionManager: MPCSessionManager) {
        super.init()
        
        self.myPeer = myPeer
        self.mpcGraph = mpcGraph
        self.mpcSessionManager = mpcSessionManager
        
        mpcBrowser = MCNearbyServiceBrowser(peer: myPeer, serviceType: "chatbox-mpc")
        mpcBrowser.delegate = self
    }
    
    func shouldChoosePeer(peer: MCPeerID, fromGraph graphID: String) -> Bool {
    
        if mpcGraph.getSize() == 0 {
            // I am disconnected
        
            if graphID == "" {
                // He is disconnected
                // The peer with the lower ID should invite the other
                return myPeer.displayName < peer.displayName
            } else {
                // He is connected
                return true
            }
        
        } else {
            // I am connected
            
            if graphID == "" {
                // He is disconnected. He should invite me.
                return false
            } else if graphID != mpcGraph.graphID || mpcGraph.hasBottleneckToPeer(peer.displayName) {
                // We're in different graphs or there's a bottleneck path to him
                // The peer with the lower ID should invite the other
                return myPeer.displayName < peer.displayName
            }
        }
        
        // We're in the same graph with redundant connections
        return false
    }
    
    //////////////////////////////////////////
    // MARK: MCNearbyServiceBrowserDelegate //
    //////////////////////////////////////////
    
    func browser(browser: MCNearbyServiceBrowser, foundPeer peer: MCPeerID, withDiscoveryInfo info: [String: String]?) {
    
        let graphID = info?["graphID"]
        if graphID != nil {
        
            // If this is the first peer found, start a timer to collect peers
            if foundPeerIDToGraphID.isEmpty {
                peerCollectionTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(MPCGraphBrowser.chooseAndInvitePeers), userInfo: nil, repeats: false)
            }
        
            // Remeber the graphID for each peerID
            foundPeerIDToGraphID[peer.displayName] = graphID!
            
            // Remember all the peers for each graphID
            var peersInGraph = foundGraphIDToPeers[graphID!]
            if peersInGraph == nil {
                peersInGraph = Set<MCPeerID>()
                foundGraphIDToPeers[graphID!] = peersInGraph
            }
            peersInGraph!.insert(peer)
            foundGraphIDToPeers[graphID!] = peersInGraph!
        }
    }
    
    
    func browser(browser: MCNearbyServiceBrowser, lostPeer peer: MCPeerID) {
    
        // Remove peer from its set of found peers for its graph
        let graphID = foundPeerIDToGraphID[peer.displayName]
        if graphID != nil {
            var foundPeersInGraph = foundGraphIDToPeers[graphID!]
            if foundPeersInGraph != nil {
                foundPeersInGraph!.remove(peer)
                
                // Remove this graphID if it's now empty
                if foundPeersInGraph!.isEmpty {
                    foundGraphIDToPeers.removeValueForKey(graphID!)
                } else {
                    foundGraphIDToPeers[graphID!] = foundPeersInGraph!
                }
            }
        }
        
        // Remove peer from mapping of peerID to graphID
        foundPeerIDToGraphID.removeValueForKey(peer.displayName)
        
        // If all the peers are lost, cancel the timer
        if foundPeerIDToGraphID.isEmpty && peerCollectionTimer != nil && peerCollectionTimer!.valid {
            peerCollectionTimer!.invalidate()
            peerCollectionTimer = nil
        }
    }
    
    
    func browser(browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: NSError) {
        print(error.localizedDescription)
    }
    
    func chooseAndInvitePeers() {
    
        print("Choosing ==================")
        for (graphID, peers) in foundGraphIDToPeers {
        
            var countForGraph = 0
            print(graphID)
            for peer in peers {
            
                // Choose up to _ people per graph (unless they're unconnected)
                if countForGraph == 3 && graphID != "" { break }
                
                if shouldChoosePeer(peer, fromGraph: graphID) {
                    print("  " + peer.displayName)
                    mpcSessionManager.invitePeer(peer, withBrowser: mpcBrowser)
                    countForGraph += 1
                }
            }
        }
        print("Done Choosing =============")
    }
}
