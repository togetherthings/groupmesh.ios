//
//  ConstraintManager.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 7/29/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit


class ConstraintManager: NSObject {
    
    static func completeAddToSubview(mainView: UIView, subView: UIView) {
        mainView.addSubview(subView)
        subView.translatesAutoresizingMaskIntoConstraints = false
        
        let leadingConstraint = NSLayoutConstraint(item: subView, attribute: .Leading, relatedBy: .Equal, toItem: mainView, attribute: .Leading, multiplier: 1, constant: 0)
        let trailingConstraint = NSLayoutConstraint(item: subView, attribute: .Trailing, relatedBy: .Equal, toItem: mainView, attribute: .Trailing, multiplier: 1, constant: 0)
        let topConstraint = NSLayoutConstraint(item: subView, attribute: .Top, relatedBy: .Equal, toItem: mainView, attribute: .Top, multiplier: 1, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: subView, attribute: .Bottom, relatedBy: .Equal, toItem: mainView, attribute: .Bottom, multiplier: 1, constant: 0)
        
        NSLayoutConstraint.activateConstraints([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
        mainView.layoutIfNeeded()
    }
}