//
//  PageViewController.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 3/24/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit

protocol PageViewControllerDelegate: class {
    
    /**
     Called when number of pages is updated.
     
     - parameter pageViewController: PageViewController instance
     - parameter count: total number of pages.
     */
    func pageViewController(tutorialPageViewController: PageViewController, didUpdatePageCount count: Int)
    
    /**
     Called when current index is updated.
     
     - parameter pageViewController: PageViewController instance
     - parameter index: currently visible page.
     */
    func pageViewController(tutorialPageViewController: PageViewController, didUpdatePageIndex index: Int)
}

class PageViewController: UIPageViewController {
    
    var pageDelegate: PageViewControllerDelegate?
    
    var orderedViewControllers: [UIViewController]!
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        delegate = self
        dataSource = self
        
        if let initialViewController = orderedViewControllers.first {
            scrollToViewController(initialViewController)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    static func newActivityViewController(activityView: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("\(activityView)ViewController")
    }
    
    private func notifyDelegateOfNewIndex() {
        if let firstViewController = viewControllers?.first, let index = orderedViewControllers.indexOf(firstViewController) {
            pageDelegate?.pageViewController(self, didUpdatePageIndex: index)
        }
    }
    
    func scrollToViewController(index newIndex: Int) {
        if let firstViewController = viewControllers?.first,
            let currentIndex = orderedViewControllers.indexOf(firstViewController) {
                let direction: UIPageViewControllerNavigationDirection = newIndex >= currentIndex ? .Forward : .Reverse
                let nextViewController = orderedViewControllers[newIndex]
                scrollToViewController(nextViewController, direction: direction)
        }
    }
    
    private func scrollToViewController(viewController: UIViewController, direction: UIPageViewControllerNavigationDirection = .Forward) {
        setViewControllers([viewController], direction: direction, animated: true, completion: { (finished) -> Void in
            self.notifyDelegateOfNewIndex()
        })
    }
}

// MARK: UIPageViewControllerDelegate

extension PageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        notifyDelegateOfNewIndex()
    }
}

// MARK: UIPageViewControllerDataSource

extension PageViewController: UIPageViewControllerDataSource {

    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
            guard let viewControllerIndex = orderedViewControllers.indexOf(viewController) else {
                return nil
            }
            
            let previousIndex = viewControllerIndex - 1
            
            guard previousIndex >= 0 else {
                return nil
            }
            
            guard orderedViewControllers.count > previousIndex else {
                return nil
            }
            
            return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
            guard let viewControllerIndex = orderedViewControllers.indexOf(viewController) else {
                return nil
            }
            
            let nextIndex = viewControllerIndex + 1
            let orderedViewControllersCount = orderedViewControllers.count
            
            guard orderedViewControllersCount != nextIndex else {
                return nil
            }
            
            guard orderedViewControllersCount > nextIndex else {
                return nil
            }
            
            return orderedViewControllers[nextIndex]
    }
}