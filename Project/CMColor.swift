//
//  CMColors.swift
//  GroupMesh
//
//  Created by Leo Da Costa on 7/29/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit

class CMColor: NSObject {
    
    static let coldBlue = UIColor(red: 108.0/255.0, green: 151.0/255.0, blue: 202.0/255.0, alpha: 1.0)
    static let lightGray = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237/255.0, alpha: 1.0)
}