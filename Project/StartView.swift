//
//  StartView.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 3/14/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit
import CoreData

var user: User!
var person = [NSManagedObject]()
var locationManager = LocationManager()

class StartView: UIViewController {

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        locationManager.start()
        
        // Core Data user identification process
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "User")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            person = results as! [NSManagedObject]
            print("(Start View) Person count: " + String(person.count))
            if person.count == 0 {
                user = User()
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let viewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("SignUp") as! SignUpView
                    self.presentViewController(viewController, animated: false, completion: nil)
                })
            }
            else {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let viewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Root") as! RootViewController
                    self.presentViewController(viewController, animated: false, completion: nil)
                })
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
}
