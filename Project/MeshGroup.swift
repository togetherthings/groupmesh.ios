//
//  MeshGroup.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 1/2/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class MeshGroup: NSObject {
    
    var name: String = ""
    var members: [AnyObject] = []
    var session: MCSession!
    
    override init() {
        super.init()
    }
    
    init(name: String) {
        super.init()
        self.name = name
    }
    
    func setGroupMesh(group: [AnyObject]) -> Void {
        members = group
    }
    
    func addMember(member: AnyObject) {
        members.append(member)
    }
    
    func addSession(session: MCSession) {
        self.session = session
    }
    
    func getSession() -> MCSession {
        return session
    }
    
    func getLength() -> Int {
        return members.count
    }
}
