//
//  CMConstants.swift
//  GroupMesh
//
//  Created by Leo Da Costa on 7/30/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit

class CMConstant: NSObject {
    
    static let cornerRadius: CGFloat = 3
}