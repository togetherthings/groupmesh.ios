//
//  ImageManager.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 1/29/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit
import Photos
import ImageIO
import AVFoundation

typealias Byte = UInt8

class ImageManager: NSObject {
    
    var imageInQueue: UIImage?
    var videoInQueue: UIView?
    var assetInQueue: PHAsset?
    
    static func setTextOnImage(text: NSString, image: UIImage, color: UIColor, withTextSize: CGFloat) -> UIImage {
        // Set up text
        let textColor = color
        let textFont = UIFont(name: "Helvetica Bold", size: withTextSize)!
        
        //Setup the image context using the passed image.
        UIGraphicsBeginImageContext(image.size)
        
        let textFontAttributes = [NSFontAttributeName: textFont, NSForegroundColorAttributeName: textColor]
        
        //Put the image into a rectangle as large as the original image
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        
        // Create point in image
        let textSize = text.sizeWithAttributes(textFontAttributes)
        let rect: CGRect = CGRectMake(image.size.width / 2 - textSize.width / 2, (image.size.height / 2 - textSize.height / 2) + 3, image.size.width / 2 + textSize.width / 2, image.size.height - textSize.height)
        
        // Draw text in image
        text.drawInRect(rect, withAttributes: textFontAttributes)
        
        // Get new image
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // End context
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    static func setTextOnImage(text: NSString, image: UIImage, color: UIColor, atPoint: CGPoint, withTextSize: CGFloat) -> UIImage {
        // Set up text
        let textColor = color
        let textFont = UIFont(name: "Helvetica Bold", size: withTextSize)!
        
        //Setup the image context using the passed image.
        UIGraphicsBeginImageContext(image.size)
        
        let textFontAttributes = [NSFontAttributeName: textFont, NSForegroundColorAttributeName: textColor]
        
        //Put the image into a rectangle as large as the original image
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        
        // Create point in image
        let rect: CGRect = CGRectMake(atPoint.x, atPoint.y, image.size.width, image.size.height)
        
        // Draw text in image
        text.drawInRect(rect, withAttributes: textFontAttributes)
        
        // Get new image
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // End context
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    static func resizeImage(image:UIImage, toTheSize size:CGSize) -> UIImage {
        
        let scale = CGFloat(max(size.width/image.size.width, size.height/image.size.height))
        let width:CGFloat  = image.size.width * scale
        let height:CGFloat = image.size.height * scale;
        let rr:CGRect = CGRectMake( 0, 0, width, height);
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0);
        image.drawInRect(rr)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        return newImage
    }
    
    static func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRectMake(0, 0, size.width, size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    
    //////////////////////////////////////////////////////
    /*                      UPDATE                      */
    //////////////////////////////////////////////////////
    
    
    func completeImagePicker(controller imagePickerController: UIImagePickerController!, dictionary: [NSObject : AnyObject]!, imageView: UIImageView) {
        imagePickerController.dismissViewControllerAnimated(true, completion: nil)
        
        let rect = getScaleByAspectRatio(dictionary[UIImagePickerControllerOriginalImage] as! UIImage, imageView: imageView)
        
        guard let assetURL = dictionary[UIImagePickerControllerReferenceURL] as? NSURL else {
            if let image = dictionary[UIImagePickerControllerOriginalImage] as? UIImage {
                imageView.image = resizeImageWithCoreGraphics(image, rect: rect)
            }
            return
        }
        
        // Fetch Asset(s)
        let fetchResult = PHAsset.fetchAssetsWithALAssetURLs([assetURL], options: nil)
        let options = PHImageRequestOptions()
        options.synchronous = true
        options.resizeMode = .Fast
        options.deliveryMode = .HighQualityFormat
        
        guard let asset = fetchResult.firstObject as? PHAsset else {
            if let image = dictionary[UIImagePickerControllerOriginalImage] as? UIImage {
                imageView.image = resizeImageWithCoreGraphics(image, rect: rect)
            }
            return
        }
        
        let targetSize = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
        
        PHImageManager.defaultManager().requestImageForAsset(asset, targetSize: targetSize, contentMode: PHImageContentMode.AspectFit, options: options, resultHandler: { (image, _) -> Void in
            self.loadAsset(asset)
            imageView.image = image
        })
    }
    
    func getScaleByAspectRatio(image: UIImage, imageView: UIImageView) -> CGRect {
        return AVMakeRectWithAspectRatioInsideRect(image.size, imageView.bounds)
    }
    
    func getScaleByFactor(image: UIImage) -> CGSize {
        return CGSizeApplyAffineTransform(image.size, CGAffineTransformMakeScale(0.5, 0.5))
    }
    
    func resizeImageToImageView(image: UIImage, imageView: UIImageView) -> UIImage {
        var imageWidth = image.size.width
        var imageHeight = image.size.height
        
        let imageViewWidth = imageView.frame.size.width
        let imageViewHeight = imageView.frame.size.height
        
        var imageRatio = imageWidth / imageHeight
        let imageViewRatio = imageViewWidth / imageViewHeight
        let compressionQuality: CGFloat = 0.9
        
        if imageWidth > imageViewWidth || imageHeight > imageViewHeight {
            if imageRatio < imageViewRatio {
                imageRatio = imageViewHeight / imageHeight
                imageWidth = imageRatio * imageWidth
                imageHeight = imageViewHeight
            }
            else if imageRatio > imageViewRatio {
                imageRatio = imageViewWidth / imageWidth
                imageHeight = imageRatio * imageHeight
                imageWidth = imageViewWidth
            }
            else {
                imageWidth = imageViewWidth
                imageHeight = imageViewHeight
            }
        }
        
        let rect = CGRectMake(0, 0, imageWidth, imageHeight)
        print(rect)
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        image.drawInRect(rect)
        let placeholder = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = UIImageJPEGRepresentation(placeholder, compressionQuality)
        UIGraphicsEndImageContext()
        
        print(imageData?.bytes)
        return UIImage(data: imageData!)!
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func aspectFitRect(outerRect outerRect:CGRect, innerRect:CGRect) -> CGRect {
        
        let innerRectRatio = innerRect.size.width/innerRect.size.height; // inner rect ratio
        let outerRectRatio = outerRect.size.width/outerRect.size.height; // outer rect ratio
        
        // calculate scaling ratio based on the width:height ratio of the rects.
        let ratio = (innerRectRatio > outerRectRatio) ? outerRect.size.width/innerRect.size.width:outerRect.size.height/innerRect.size.height;
        
        // The x-offset of the inner rect as it gets centered
        let xOffset = (outerRect.size.width-(innerRect.size.width*ratio))*0.5;
        
        // The y-offset of the inner rect as it gets centered
        let yOffset = (outerRect.size.height-(innerRect.size.height*ratio))*0.5;
        
        // aspect fitted origin and size
        let innerRectOrigin = CGPoint(x: xOffset+outerRect.origin.x, y: yOffset+outerRect.origin.y);
        let innerRectSize = CGSize(width: innerRect.size.width*ratio, height: innerRect.size.height*ratio);
        
        return CGRect(origin: innerRectOrigin, size: innerRectSize);
    }
    
    // Load / Save Media
    
    func loadImage(image: UIImage) {
        imageInQueue = image
    }
    
    func loadVideo(video: UIView) {
        videoInQueue = video
    }
    
    func loadAsset(asset: PHAsset) {
        assetInQueue = asset
    }
    
    /*func saveImageToAssets(inout assets: [PHAsset], image: UIImage) {
        // Create Asset, Obtain Placeholder
        let createAssetRequest = PHAssetChangeRequest.creationRequestForAssetFromImage(image)
        let assetPlaceholder = createAssetRequest.placeholderForCreatedAsset
        
        // Set collection
        let collection = user.hidden.imageAssetCollection
        let assetsChangeRequest = PHAssetCollectionChangeRequest(forAssetCollection: collection)
        assetsChangeRequest!.addAssets([assetPlaceholder!])
        
        // Fetch Result
        let fetchResult = PHAsset.fetchAssetsInAssetCollection(collection, options: nil)
        let asset = fetchResult.lastObject as! PHAsset
        assets.append(asset)
    }*/
    
    func saveAsset(inout assets: [PHAsset]) {
        assets.append(assetInQueue!)
    }
    
    // Image I/O
    
    func resizeImageURLWithImageIO(url: NSURL, width: CGFloat, height: CGFloat) -> UIImage {
        if let imageSource = CGImageSourceCreateWithURL(url, nil) {
            let options: [NSString: NSObject] = [
                kCGImageSourceThumbnailMaxPixelSize: max(width, height) / 2.0,
                kCGImageSourceCreateThumbnailFromImageAlways: true
            ]
            
            return CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options).flatMap { UIImage(CGImage: $0) }!
        }
        else {
            return UIImage()
        }
    }
    
    // Core Graphics
    
    func resizeImageWithCoreGraphics(image: UIImage, rect: CGRect) -> UIImage {
        let cgImage = image.CGImage
        let width = CGImageGetWidth(cgImage) / 2
        let height = CGImageGetHeight(cgImage) / 2
        let bitsPerComponent = CGImageGetBitsPerComponent(cgImage)
        let bytesPerRow = CGImageGetBytesPerRow(cgImage)
        let colorSpace = CGImageGetColorSpace(cgImage)
        let bitmapInfo = CGImageGetBitmapInfo(cgImage)
        
        let context = CGBitmapContextCreate(nil, width, height, bitsPerComponent, bytesPerRow, colorSpace, bitmapInfo.rawValue)
        
        CGContextSetInterpolationQuality(context, CGInterpolationQuality.High)
        
        CGContextDrawImage(context, rect, cgImage)
        
        return CGBitmapContextCreateImage(context).flatMap { UIImage(CGImage: $0) }!
    }
    
    
    // MARK: Class functions
    
    static func screenAdjustedSize(view: UIView) -> CGSize {
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        let screenHeight = UIScreen.mainScreen().bounds.size.height
        return scaleByProportion(view.frame.size, adjustToSize: CGSizeMake(screenWidth, screenHeight))
    }
    
    @nonobjc
    static func screenAdjustedSize(size: CGSize) -> CGSize {
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        let screenHeight = UIScreen.mainScreen().bounds.size.height
        return scaleByProportion(size, adjustToSize: CGSizeMake(screenWidth, screenHeight))
    }
    
    static func scaleByProportion(size: CGSize, adjustToSize: CGSize) -> CGSize {
        let width = size.width
        let height = size.height
        let viewWidth = adjustToSize.width
        let viewHeight = adjustToSize.height
        let adjustedWidth = viewWidth * (1 - exp((-viewWidth - width) / viewWidth))
        let adjustedHeight = (viewHeight * (1 - exp((-viewHeight - height) / viewHeight))) / 2
        return CGSizeMake(adjustedWidth, adjustedHeight)
    }
    
    static func setRoundedEdge(view: UIView) {
        let width = view.frame.size.width
        let height = view.frame.size.height
        let hypothenus = sqrt(pow(width, 2) + pow(height, 2))
        let radius = sqrt(pow(hypothenus - width, 2) + pow(hypothenus - height, 2))
        view.layer.cornerRadius = radius - (radius * 0.0099999999999)
    }
}
