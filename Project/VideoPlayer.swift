//
//  VideoPlayer.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 5/22/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlayer: NSObject {
    
    static let MAX_DURATION = NSTimeInterval(30)
    
    var player = AVPlayer()
    var avPlayerLayer: AVPlayerLayer!
    var timerLabel = UILabel()
    var seekSlider = UISlider()
    var playerRateBeforeSeek: Float = 0
    
    private var playbackLikelyToKeepUpContext = UnsafeMutablePointer<(Void)>(nil)
    
    let loadingIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
    
    override init() {
        super.init()
        avPlayerLayer = AVPlayerLayer(player: player)
        player.addObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp", options: NSKeyValueObservingOptions.New, context: playbackLikelyToKeepUpContext)
    }
    
    func createVideoView(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) -> UIView {
        let rect = CGRectMake(x, y, width, height)
        let videoView = UIView(frame: rect)
        
        let height: CGFloat = 30
        let y: CGFloat = videoView.bounds.size.height - height
        
        // Timer
        timerLabel.textColor = UIColor.whiteColor()
        timerLabel.frame = CGRect(x: 5, y: y, width: 60, height: height)
        
        // Seek Slider
        seekSlider.frame = CGRect(x: timerLabel.frame.origin.x + timerLabel.bounds.size.width, y: y, width: videoView.bounds.size.width - timerLabel.bounds.size.width - 5, height: height)
        seekSlider.addTarget(self, action: #selector(VideoPlayer.sliderBeganTracking(_:)), forControlEvents: UIControlEvents.TouchDown)
        seekSlider.addTarget(self, action: #selector(VideoPlayer.sliderEndedTracking(_:)), forControlEvents: [UIControlEvents.TouchUpInside, UIControlEvents.TouchUpOutside])
        seekSlider.addTarget(self, action: #selector(VideoPlayer.sliderValueChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        // Buffer Indicator
        loadingIndicatorView.center = CGPoint(x: CGRectGetMidX(videoView.bounds), y: CGRectGetMidY(videoView.bounds))
        loadingIndicatorView.hidesWhenStopped = true
        
        setFootageTimer()
        avPlayerLayer.frame = videoView.bounds
        
        // Settings
        videoView.backgroundColor = UIColor.blackColor()
        videoView.layer.cornerRadius = 4
        videoView.layer.addSublayer(avPlayerLayer)
        createPlaybackButton(videoView)
        videoView.addSubview(timerLabel)
        videoView.addSubview(seekSlider)
        videoView.addSubview(loadingIndicatorView)
        
        return videoView
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if context == playbackLikelyToKeepUpContext {
            if player.currentItem!.playbackLikelyToKeepUp {
                loadingIndicatorView.stopAnimating()
            }
            else {
                loadingIndicatorView.startAnimating()
            }
        }
    }
    
    @objc private func sliderBeganTracking(slider: UISlider!) {
        playerRateBeforeSeek = player.rate
        player.pause()
    }
    
    @objc private func sliderEndedTracking(slider: UISlider!) {
        let videoDuration = CMTimeGetSeconds(player.currentItem!.duration)
        let elapsedTime: Float64 = videoDuration * Float64(slider.value)
        updateTimeLabel(elapsedTime, duration: videoDuration)
        
        player.seekToTime(CMTimeMakeWithSeconds(elapsedTime, 10)) { (completed: Bool) -> Void in
            if (self.playerRateBeforeSeek > 0) {
                self.player.play()
            }
        }
    }
    
    @objc private func sliderValueChanged(slider: UISlider!) {
        let videoDuration = CMTimeGetSeconds(player.currentItem!.duration)
        let elapsedTime: Float64 = videoDuration * Float64(slider.value)
        updateTimeLabel(elapsedTime, duration: videoDuration)
    }
    
    private func setFootageTimer() {
        let timeInterval: CMTime = CMTimeMakeWithSeconds(1.0, 10)
        player.addPeriodicTimeObserverForInterval(timeInterval, queue: dispatch_get_main_queue()) { (elapsedTime: CMTime) -> Void in
            self.observeTime(elapsedTime) };
    }
    
    private func observeTime(elapsedTime: CMTime) {
        let duration = CMTimeGetSeconds(player.currentItem!.duration);
        if (isfinite(duration)) {
            let elapsedTime = CMTimeGetSeconds(elapsedTime)
            updateTimeLabel(elapsedTime, duration: duration)
        }
    }
    
    private func updateTimeLabel(elapsedTime: Float64, duration: Float64) {
        let timeRemaining: Float64 = CMTimeGetSeconds(player.currentItem!.duration) - elapsedTime
        timerLabel.text = String(format: "%02d:%02d", ((lround(timeRemaining) / 60) % 60), lround(timeRemaining) % 60)
        updateSliderPosition(elapsedTime, duration: duration)
    }
    
    private func updateSliderPosition(elapsedTime: Float64, duration: Float64) {
        let value = Float(elapsedTime / duration)
        seekSlider.setValue(value, animated: true)
    }
    
    private func createPlaybackButton(videoView: UIView) {
        let button = UIButton()
        button.frame = videoView.bounds
        button.addTarget(self, action: #selector(VideoPlayer.playbackButtonTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        videoView.addSubview(button)
    }
    
    @objc private func playbackButtonTapped(sender: UIButton) {
        if player.rate > 0 {
            player.pause()
        }
        else {
            player.play()
        }
    }
    
    deinit {
        player.removeObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp", context: playbackLikelyToKeepUpContext)
    }
}