//
//  ActivityTableCell.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 5/30/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class ActivityTableCell: CustomTableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var customImageView: UIImageView!
    @IBOutlet weak var textView: CMTextView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaView: CMMediaView!
    @IBOutlet weak var mediaViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var detailViewHeightConstraint: NSLayoutConstraint!
    
    
    let lowLayoutPriority: Float = 250
    let highLayoutPriority: Float = 999
    
    var hasImageView = false
    var hasVideoView = false
    
    var detailViewDefaultHeight: CGFloat = 48
    var showsDetails = false {
        didSet {
            if showsDetails {
                detailViewHeightConstraint.constant = detailViewDefaultHeight
            }
            detailViewHeightConstraint.priority = showsDetails ? lowLayoutPriority : highLayoutPriority
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        detailViewHeightConstraint.constant = 0
        textViewHeightConstraint.constant = 0
        mediaViewHeightConstraint.constant = 0
        setCustomImageView()
        updateLayout()
        resetViews()
    }
    
    func setView(new: Bool, localMessage: LocalMessage, dataCache cache: NSMutableDictionary, inout assets: [PHAsset]) {
        if new {
            dateLabel.text = localMessage.getTimeStamp()
            resetViews()
        }
        textView.setContentHuggingPriority(lowLayoutPriority, forAxis: .Vertical)
        mediaView.setContentHuggingPriority(lowLayoutPriority, forAxis: .Vertical)
        setInitialValues(localMessage.getAuthor(), displayImage: localMessage.displayImage)
        if let message = Optional(localMessage.getMessage()) where message.characters.count > 0 {
            let cacheKey = cache.count
            textViewHeightConstraint.constant = 55
            textView.setContentHuggingPriority(highLayoutPriority, forAxis: .Vertical)
            manageTextView(new, message: localMessage.getMessage(), key: cacheKey, cache: cache)
        }
        if localMessage.media?.count > 0 {
            let assetKey = assets.count
            mediaView.setContentHuggingPriority(highLayoutPriority, forAxis: .Vertical)
            manageMediaView(new, dataType: localMessage.mediaType!, key: assetKey, assets: &assets)
        }
        updateLayout()
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        switch(keyPath!) {
        case "image":
            let imageWidth = customImageView.frame.width
            let imageHeight = customImageView.frame.height
            let radius = (imageWidth + imageHeight) / 4
            customImageView.layer.cornerRadius =  radius
        default: return
        }
    }

    
    // MARK: - UIButton Action
    
    @IBAction func replyAction(sender: AnyObject) {
        
    }
    
    @IBAction func relayAction(sender: AnyObject) {
        
    }
   
    @IBAction func historyAction(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("LMPopover", object: nil)
    }
    
    ////////////////////////////
    
    private func setCustomImageView() {
        let adjustedSize = ImageManager.screenAdjustedSize(customImageView)
        customImageView.contentMode = .ScaleAspectFit
        customImageView.layer.masksToBounds = true
        customImageView.clipsToBounds = true
        customImageView.frame.size.width = adjustedSize.width
        customImageView.frame.size.height = adjustedSize.height
        ImageManager.setRoundedEdge(customImageView)
        customImageView.addObserver(self, forKeyPath: "image", options: .New, context: nil)
    }
    
    private func setNewText(message: AnyObject, key: Int, cache: NSMutableDictionary) {
        let message = message as! String
        textView.text = message
        cache.setObject(message, forKey: key)
    }
    
    private func setNewImage(inout assets: [PHAsset], path: Int) {
        imageManager!.saveAsset(&assets)
        setMedia(assets, path: path)
    }
    
    private func setNewVideo(message: AnyObject, key: Int, cache: NSMutableDictionary) {
        
    }
    
    private func setViewType(dataType: DataType) {
        hasImageView = false
        hasVideoView = false
        switch dataType {
        case .Image:
            hasImageView = true
        case .Video:
            hasVideoView = true
        default: break
        }
    }
    
    private func setInitialValues(author: String, displayImage: UIImage) {
        nameLabel.text = author
        customImageView.image = displayImage
    }
    
    private func manageTextView(new: Bool, message: String?, key: Int, cache: NSMutableDictionary) {
        if new {
            setNewText(message!, key: key, cache: cache)
        }
        else {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let text = cache.objectForKey(key - 1) as? String {
                    self.textView.text = text
                }
            })
        }
    }
    
    private func manageMediaView(new: Bool, dataType: DataType, key: Int, inout assets: [PHAsset]) {
        setMediaContainer(mediaView)
        switch dataType {
        case .Image:
            if !hasImageView {
                compileViewWithAsset(withAsset: &assets, atPath: key, asType: dataType)
                setViewType(dataType)
            }
            else if new {
                setNewImage(&assets, path: key)
            }
            else {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.setMediaWithAsset(&assets, path: key - 1)
                })
            }
        case .Video:
            if !hasVideoView {
                compileViewWithAsset(withAsset: &assets, atPath: key, asType: dataType)
                setViewType(dataType)
            }
            else if new {
                //setNewVideo(message, key: key, cache: cache!)
            }
            else {
                /*dispatch_async(dispatch_get_main_queue(), { () -> Void in
                 var videoURL = cache.objectForKey(path) as! NSURL
                 let playerItem = AVPlayerItem(URL: videoURL)
                 let videoPlayer = self.mediaView.viewWithTag(tag.rawValue) as! VideoPlayer
                 videoPlayer.player.replaceCurrentItemWithPlayerItem(playerItem)
                 videoPlayer.loadingIndicatorView.startAnimating()
                 videoPlayer.player.pause()
                 })*/
            }
        default: break
        }
    }
    
    private func resetViews() {
        textView.text = ""
        mediaView.clear()
    }
    
    private func updateLayout() {
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
}
