//
//  ActivityTemplateController.swift
//  ChatBox
//
//  Created by Leo Da Costa on 6/24/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices

class ActivityTemplateController: UIViewController {
    
    var textCache = NSMutableDictionary()
    
    /*********************************
     *          ASSET CACHE          *
     *********************************/
    
    var null: [PHAsset] = []
    
    let cachingImageManager = PHCachingImageManager()
    let cachingVideoManager = PHCachingImageManager()
    
    var imageAssets: [PHAsset] = [] {
        willSet {
            cachingImageManager.stopCachingImagesForAllAssets()
        }
        didSet {
            cachingImageManager.startCachingImagesForAssets(self.imageAssets,
                                                            targetSize: PHImageManagerMaximumSize,
                                                            contentMode: .AspectFit,
                                                            options: nil
            )
        }
    }
    
    var videoAssets: [PHAsset] = [] {
        willSet {
            cachingVideoManager.stopCachingImagesForAllAssets()
        }
        didSet {
            cachingVideoManager.startCachingImagesForAssets(self.imageAssets,
                                                            targetSize: PHImageManagerMaximumSize,
                                                            contentMode: .AspectFit,
                                                            options: nil
            )
        }
    }
    
    /*************************
     *          END          *
     *************************/
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
