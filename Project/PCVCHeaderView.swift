//
//  PCVCHeaderView.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 3/20/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

class PCVCHeaderView: UICollectionReusableView {
    @IBOutlet weak var titleLabel: UILabel!
}
