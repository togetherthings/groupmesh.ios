//
//  LMPopoverActivity.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 5/19/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

class LMPopoverActivity: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
