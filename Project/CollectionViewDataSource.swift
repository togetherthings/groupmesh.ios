//
//  CollectionViewDataSource.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 3/18/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit
import MultipeerConnectivity

protocol ContentAwareCollectionViewDataSource {
    func configuredCellForIndexPath(indexPath: NSIndexPath, prototype: Bool) -> UICollectionViewCell
}

class CollectionViewDataSource: NSObject {
    
    var data = [Data]()
    var prototypeCells = NSCache()
    var tableView: UITableView!
    var collectionView: UICollectionView!
    var cell: ProfileTableCell!
    var title: String!
    var type: String!
    
    
    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
    }
    
    init(collectionView: UICollectionView, type: String) {
        self.collectionView = collectionView
        self.type = type
    }
    
    init(collectionView: UICollectionView, cell: ProfileTableCell) {
        self.collectionView = collectionView
        self.cell = cell
        type = "PCVCHeaderView"
    }
    
    func addData(data: [String]) {
        var asData = [Data]()
        for info in data {
            let infoData = Data(title: "Data", text: info)
            asData.append(infoData)
        }
        self.data = asData
        collectionView.reloadData()
    }
    
    func populateData(data: [String : AnyObject]) {
        var asData = [Data]()
        let array = data["data"] as! [AnyObject]
        for object in array {
            let objectData = Data(title: "Data")
            var info = [String : AnyObject]()
            if let dataType = data["class"] where dataType as! String == "MCPeerID" {
                let peer = object as! MCPeerID
                info["image"] = peer.displayImage
                objectData.data = info
                objectData.text = peer.displayName
            }
            asData.append(objectData)
        }
        self.data = asData
        collectionView.reloadData()
    }
    
    func instantiateProfileData(data: [AnyObject]) {
        var asData = [Data]()
        for info in data {
            let infoData = Data(title: "Data", text: info as! String)
            asData.append(infoData)
        }
        let previousCount = self.data.count
        self.data = asData
        collectionView.reloadData()
        if previousCount != asData.count {
            resizeCollectionView()
        }
    }
    
    func resizeCollectionView() {
        var index = 0; if cell.getTitle() == "Interests" { index = 1 }
        let size = collectionView.collectionViewLayout.collectionViewContentSize()
        collectionView.frame = CGRect(x: 8, y: 8, width: size.width, height: size.height)
        collectionView.setNeedsUpdateConstraints()
        collectionView.layoutIfNeeded()
        cell.height = size.height
        let data = ["cell" : index, "height" : size.height]
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName(UIContentSizeCategoryDidChangeNotification, object: data)
        })
    }
}

// MARK: - UICollectionViewDataSource

extension CollectionViewDataSource: UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = configuredCellForIndexPath(indexPath, prototype: false)
        cell.setNeedsLayout()
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        switch type {
        case "PCVCHeaderView":
            let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: type, forIndexPath: indexPath) as! PCVCHeaderView
            headerView.titleLabel.text = title
            return headerView
        default:
            return collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: type, forIndexPath: indexPath)
        }
    }
}

// MARK: - Cell configuration

extension CollectionViewDataSource: ContentAwareCollectionViewDataSource {
    
    func configuredCellForIndexPath(indexPath: NSIndexPath, prototype: Bool) -> UICollectionViewCell {
        let cell = reusableCellForIndexPath(indexPath, prototype: prototype) as! CollectionViewCell
        let model: AnyObject = data[indexPath.row]
        cell.configure(model: model, prototype: prototype)
        
        return cell
    }
    
    private func cellIdentifierForIndexPath(indexPath: NSIndexPath) -> String {
        let model = data[indexPath.row]
        let type = model.title
        switch type {
        case "Data":
            return CollectionViewCell.Const.ReuseIdentifier
        default:
            abort()
        }
    }
    
    private func reusableCellForIndexPath(indexPath: NSIndexPath, prototype: Bool) -> UICollectionViewCell {
        let cellId = cellIdentifierForIndexPath(indexPath)
        
        if prototype {
            return prototypeCellForIdentifier(cellId)
        }
        
        return collectionView.dequeueReusableCellWithReuseIdentifier(cellId, forIndexPath: indexPath)
    }
    
    private func prototypeCellForIdentifier(reuseIdentifier: String) -> UICollectionViewCell {
        if let cell = prototypeCells.objectForKey(reuseIdentifier) as? UICollectionViewCell {
            return cell
        }
        
        // Create new cell for reuseIdentifier
        var cell: UICollectionViewCell
        switch reuseIdentifier {
        case CollectionViewCell.Const.ReuseIdentifier:
            cell = CollectionViewCell()
        default:
            abort()
        }
        
        // Cache it
        prototypeCells.setObject(cell, forKey: reuseIdentifier)
        
        return cell
    }
}

