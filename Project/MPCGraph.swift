//
//  BTGraph.swift
//  ChatBox
//
//  Created by Jonathan Noyola on 5/18/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import MultipeerConnectivity

class MPCGraph: NSObject {

    var mpcSessionManager: MPCSessionManager!

    var mpcGraphBrowser: MPCGraphBrowser!
    
    var peerIDGraph = [String: Set<String>]()
    
    var graphID = ""
    
    override init() {
        super.init()
    
        let myPeer = MCPeerID(displayName: "name")//user.getFullName())
        mpcSessionManager = MPCSessionManager(myPeer: myPeer)
        mpcGraphBrowser = MPCGraphBrowser(myPeer: myPeer, mpcGraph: self, mpcSessionManager: mpcSessionManager)
        
    
//        mpcManager = MPCManager()
//        mpcManager.advertiser.startAdvertisingPeer()
//        mpcBrowser = MPCBrowser()
//        mpcManager.delegate = mpcBrowser
//        mpcManager.browser.startBrowsingForPeers()
    }
    
    func getSize() -> Int {
        return peerIDGraph.count
    }
    
    func hasBottleneckToPeer(peerID: String) -> Bool {
        return true
    }
    
    func foundPeer() {
        
    }
    
    func lostPeer() {
        
    }
}
