//
//  ActivityView.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 3/24/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices

class ActivityView: ActivityTemplateController, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    struct Static {
        static var dispatchOnceToken: dispatch_once_t = 0
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    var imageManager = ImageManager()
    var localMessages = [String : [LocalMessage]]()
    var imagePicker: UIImagePickerController!
    var videoPicker: UIImagePickerController!
    
    var newPost = false
    
    var expandedIndexPath: NSIndexPath? {
        didSet {
            switch expandedIndexPath {
            case .Some(let index):
                tableView.reloadRowsAtIndexPaths([index], withRowAnimation: UITableViewRowAnimation.Automatic)
            case .None:
                tableView.reloadRowsAtIndexPaths([oldValue!], withRowAnimation: UITableViewRowAnimation.Automatic)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ActivityView.postMessage(_:)), name: "receivedLocalMessage", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ActivityView.openThought), name: "openThought", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ActivityView.LMPopover(_:)), name: "LMPopover", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ActivityView.OTCall), name: "openThought-calls", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PeopleView.populatePeers(_:)), name: "Peers", object: nil)
        
        // Media
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        
        videoPicker = UIImagePickerController()
        videoPicker.delegate = self
        videoPicker.allowsEditing = false
        videoPicker.sourceType = .Camera
        videoPicker.mediaTypes = [kUTTypeMovie as String]
        videoPicker.videoMaximumDuration = VideoPlayer.MAX_DURATION
        
        tableView.contentInset.top = 0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 400
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: UITableView related method implementation
    
    func calculateHeightForConfiguredCellSizingCell(cell: ActivityTableCell) -> CGFloat {
        let size = cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
        return size.height
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let messages = Array(localMessages.values)
        return messages.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("localMessage", forIndexPath: indexPath) as! ActivityTableCell
        cell.imageManager = self.imageManager
        
        /*let paths = tableView.indexPathsForVisibleRows
        if ((paths?.contains(indexPath)) != nil) {}*/
        
        let messages = Array(localMessages.values)
        let message = messages[indexPath.row].last!
        let dataType = message.mediaType
        
        switch dataType! {
        case .Text:
            cell.setView(newPost, localMessage: message, dataCache: textCache, assets: &null)
        case .Image:
            cell.setView(newPost, localMessage: message, dataCache: textCache, assets: &imageAssets)
        case .Video:
            cell.setView(newPost, localMessage: message, dataCache: textCache, assets: &videoAssets)
        }
        
        newPost = false
        
        switch expandedIndexPath {
        case .Some(let expandedIndexPath) where expandedIndexPath == indexPath:
            cell.showsDetails = true
        default:
            cell.showsDetails = false
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        switch expandedIndexPath {
        case .Some(_) where expandedIndexPath == indexPath:
            expandedIndexPath = nil
        case .Some(let expandedIndex) where expandedIndex != indexPath:
            expandedIndexPath = nil
            self.tableView(tableView, didSelectRowAtIndexPath: indexPath)
        default:
            expandedIndexPath = indexPath
        }
    }
    
    
    // MARK: Local Messages
    
    func postMessage(notification: NSNotification) {
        newPost = true
        let localMessage = LocalMessage(notification: notification)
        let uuid = String(user.hidden.deviceID)
        
        if localMessages[uuid] == nil {
            localMessages[uuid] = [LocalMessage]()
            localMessages[uuid]?.append(localMessage)
        }
        else {
            localMessages[uuid]?.append(localMessage)
        }
        
        tableView.reloadData()
    }
    
    // Popover
    
    func LMPopover(messages: [LocalMessage]) {
        self.performSegueWithIdentifier("showLMView", sender: self)
    }
    
    func openThought() {
        self.performSegueWithIdentifier("openThought", sender: self)
    }
    
    // Segue
    
    func populatePeers(notification: NSNotification) {
        let receivedData = notification.object as! [String : AnyObject]
        var data = [String : AnyObject]()
        data["class"] = "MCPeerID"
        if let type = receivedData["general"] {
            data["data"] = type
            NSLog("\(data)")
            //generalCollectionViewDataSource.populateData(data)
        }
        if let type = receivedData["popular"] {
            print("Popular")
            data["data"] = type
            NSLog("\(data)")
            //popularCollectionViewDataSource.populateData(data)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.identifier {
            case "showLMView"?:
                let vc = segue.destinationViewController as! LMPopoverView
                if let controller = vc.popoverPresentationController {
                    controller.delegate = self
                    let _ = vc.view
                    controller.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                    let uuid = String(user.hidden.deviceID)
                    vc.receiveLocalMessages(localMessages[uuid]!)
                    
                    let assetsList = ["images" : imageAssets, "videos" : videoAssets]
                    vc.receiveAssets(assetsList)
                }
            case "openThought"?:
                let vc = segue.destinationViewController as! OTView
                if let controller = vc.popoverPresentationController {
                    controller.delegate = self
                    vc.preferredContentSize = ImageManager.screenAdjustedSize(CGSizeMake(320, 250))
                    vc.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
                    
                    // Set Blur
                    let blurEffect = UIBlurEffect(style: .Light)
                    let blurredEffectView = UIVisualEffectView(effect: blurEffect)
                    blurredEffectView.frame = view.bounds
                    blurredEffectView.tag = 123
                    view.addSubview(blurredEffectView)
                    
                    // Load View
                    let _ = vc.view // -- Loader
                    let screenWidth = UIScreen.mainScreen().bounds.size.width
                    let screenHeight = UIScreen.mainScreen().bounds.size.height
                    let x: CGFloat = screenWidth / 2
                    let y: CGFloat = screenHeight / 2.5
                    controller.sourceRect = CGRectMake(x, y, 0, 0)
                    controller.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                    
                    vc.imageManager = self.imageManager
                }
        default: break
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        switch controller.presentedViewController {
        case is OTView:
            return .None
        default:
            return .OverFullScreen
        }
    }

    
    // MARK: OTView calls
    
    func OTCall(notification: NSNotification) {
        let call = notification.object as! String
        switch call {
            case "videoCall":
                callVideoCamera()
            case "photoCall-get":
                getPhoto()
            case "photoCall-post":
                takePhoto()
        default: break
        }
    }
    
    private func callVideoCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.Camera) {
            self.presentViewController(videoPicker, animated: true, completion: nil)
        }
        else {
            let alertController = UIAlertController(title: "Camera Inaccessible", message: "Camera cannot be accessed at this time", preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(okAction)
            dispatch_async(dispatch_get_main_queue(), { ()
                self.presentViewController(alertController, animated: true, completion: nil)
            })
        }
    }
    
    private func getPhoto() {
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    private func takePhoto() {
        imagePicker.sourceType = .Camera
        imagePicker.cameraCaptureMode = .Photo
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    
    // MARK: - UIImagePickerControllerDelegate methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        dismissViewControllerAnimated(true, completion: nil)
        openThought()
        let data = [imagePicker, info]
        NSNotificationCenter.defaultCenter().postNotificationName("receivedCallData", object: data)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
        openThought()
    }
    
    
    // MARK: - UITextView for UITableViewCell
    
    func textViewDidChange(textView: UITextView) {
        self.tableView.beginUpdates()
        textView.frame = CGRectMake(textView.frame.minX, textView.frame.minY, textView.frame.width, textView.contentSize.height + 40)
        self.tableView.endUpdates()
    }
}
