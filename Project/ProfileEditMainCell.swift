//
//  ProfileEditMainCell.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 3/8/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

class ProfileEditMainCell: UITableViewCell {
    
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var view: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Set View
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    func addPlaceHolders(data: [String : String!]) {
        firstNameTextField.placeholder = data["first"]
        lastNameTextField.placeholder = data["last"]
        emailTextField.placeholder = data["email"]
    }
    
    // Resign First Responder
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
}
