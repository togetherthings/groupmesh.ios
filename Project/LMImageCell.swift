//
//  LMImageCell.swift
//  ChatBox
//
//  Created by Leo Da Costa on 5/20/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

class LMImageCell: UITableViewCell {
    
    @IBOutlet weak var customImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
