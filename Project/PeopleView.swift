//
//  PeopleView.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 3/26/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

class PeopleView: UIViewController {
    
    @IBOutlet weak var popularCollectionView: UICollectionView!
    @IBOutlet weak var generalCollectionView: UICollectionView!
    
    var popularCollectionViewDataSource: CollectionViewDataSource!
    var generalCollectionViewDataSource: CollectionViewDataSource!
    var cellSizeCache = NSCache()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let borderWidth: CGFloat = 0.5
        let cornerRadius: CGFloat = 3
        let color = UIColor.lightGrayColor().CGColor //UIColor(red: 71.0/255.0, green: 89.0/255.0, blue: 114.0/255.0, alpha: 1.0).CGColor
        
        // Popular Collection View
        popularCollectionViewDataSource = CollectionViewDataSource(collectionView: popularCollectionView, type: "PopularHeader")
        popularCollectionView.registerClass(CollectionViewCell.self, forCellWithReuseIdentifier: CollectionViewCell.Const.ReuseIdentifier)
        popularCollectionView.delegate = self
        popularCollectionView.dataSource = popularCollectionViewDataSource
        
        popularCollectionView.layer.borderColor = color
        popularCollectionView.layer.borderWidth = borderWidth
        popularCollectionView.layer.cornerRadius = cornerRadius
        
        var layout = popularCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(1, 5, 0, 5)
        layout.minimumInteritemSpacing = 5
        
        // General Collection View
        generalCollectionViewDataSource = CollectionViewDataSource(collectionView: generalCollectionView, type: "GeneralHeader")
        generalCollectionView.registerClass(CollectionViewCell.self, forCellWithReuseIdentifier: CollectionViewCell.Const.ReuseIdentifier)
        generalCollectionView.delegate = self
        generalCollectionView.dataSource = generalCollectionViewDataSource
        
        generalCollectionView.layer.borderColor = color
        generalCollectionView.layer.borderWidth = borderWidth
        generalCollectionView.layer.cornerRadius = cornerRadius
        
        layout = generalCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(1, 5, 0, 5)
        layout.minimumInteritemSpacing = 5
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PeopleView.populatePeers(_:)), name: "Peers", object: nil)
        
        NSNotificationCenter.defaultCenter().postNotificationName("requestFoundPeers", object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func populatePeers(notification: NSNotification) {
        let receivedData = notification.object as! [String : AnyObject]
        var data = [String : AnyObject]()
        data["class"] = "MCPeerID"
        if let type = receivedData["general"] {
            data["data"] = type
            generalCollectionViewDataSource.populateData(data)
        }
        if let type = receivedData["popular"] {
            print("Popular")
            data["data"] = type
            popularCollectionViewDataSource.populateData(data)
        }
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
}

// MARK: - UICollectionViewFlowLayout Delegate

extension PeopleView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        // If fitted size was computed in the past for this cell, return it from cache
        if let size = cellSizeCache.objectForKey(indexPath) as? NSValue {
            return size.CGSizeValue()
        }
        
        let dataSource = collectionView.dataSource as! CollectionViewDataSource
        let cell = dataSource.configuredCellForIndexPath(indexPath, prototype: true) as! ContentAwareCollectionViewCell
        // Set a constrained size for the cell
        let width: CGFloat = collectionView.bounds.width / 3 - 20
        let height = CGFloat.max
        let constrainedSize = CGSize(width: width, height: height)
        let size = cell.fittedSizeForConstrainedSize(constrainedSize)
        
        cellSizeCache.setObject(NSValue(CGSize: size), forKey: indexPath)
        
        return size
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let selectedPeer = appDelegate.mpcManager.foundPeers[indexPath.row]
        appDelegate.mpcManager.browser.invitePeer(selectedPeer, toSession: appDelegate.mpcManager.session, withContext: nil, timeout: 20)
    }
}
