//
//  CustomTableViewCell.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 6/10/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit
import Photos

class CustomTableViewCell: UITableViewCell {
    
    let imageViewRect = CGRectMake(0, 0, 200, 135)
    let videoViewRect = CGRectMake(0, 0, 200, 135)
    
    var containerView: UIView!
    
    var timeStamp: String?
    var messages: [LocalMessage]?
    var viewInQueue: UIView?
    var imageManager: ImageManager?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textLabel?.hidden = true
        imageView?.hidden = true
    }
    
    func createMessageContainer() {
        messages = [LocalMessage]()
    }
    
    func setMediaContainer(view: UIView) {
        containerView = view
    }
    
    func addViewToContainer() {
        if let view = viewInQueue {
            containerView.subviews.forEach({ $0.removeFromSuperview() })
            containerView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            let leadingConstraint = NSLayoutConstraint(item: view, attribute: .Leading, relatedBy: .Equal, toItem: containerView, attribute: .Leading, multiplier: 1, constant: 0)
            let trailingConstraint = NSLayoutConstraint(item: view, attribute: .Trailing, relatedBy: .Equal, toItem: containerView, attribute: .Trailing, multiplier: 1, constant: 0)
            let topConstraint = NSLayoutConstraint(item: view, attribute: .Top, relatedBy: .Equal, toItem: containerView, attribute: .Top, multiplier: 1, constant: 0)
            let bottomConstraint = NSLayoutConstraint(item: view, attribute: .Bottom, relatedBy: .Equal, toItem: containerView, attribute: .Bottom, multiplier: 1, constant: 0)
            containerView.addConstraints([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
        }
        else {
            return
        }
    }
    
    func textViewFitToContent(textView: UITextView) {
        let fixedWidth = textView.frame.size.width
        let newSize = textView.sizeThatFits(CGSizeMake(fixedWidth, CGFloat(MAXFLOAT)))
        var newFrame = textView.frame
        newFrame.size = CGSizeMake(fmax(newSize.width, fixedWidth), newSize.height)
        textView.frame = newFrame
    }
    
    func setTextView(message: AnyObject, key: Int, cache: NSMutableDictionary) {
        let message = message as! String
        let textView = UITextView()
        cache.setObject(message, forKey: key)
        
        viewInQueue = textView
    }
    
    func setAsImageView() {
        let imageView = UIImageView(frame: imageViewRect)
        imageView.layer.cornerRadius = 4
        imageView.contentMode = .ScaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
        imageView.tag = DataTag.Image.rawValue
        
        viewInQueue = imageView
    }
    
    func setAsVideoView(message: AnyObject, key: Int, cache: NSMutableDictionary) {
        let videoPlayer = VideoPlayer()
        let videoView = videoPlayer.createVideoView(0, y: 0, width: 0, height: 0)
        videoView.tag = DataTag.Video.rawValue
        
        cache.setObject(videoPlayer, forKey: key)
        
        viewInQueue = videoView
    }
    
    func pullAsset(assets: [PHAsset], atPath: Int) -> [AnyObject?] {
        let manager = PHImageManager.defaultManager()
        
        if self.tag != 0 {
            manager.cancelImageRequest(PHImageRequestID(self.tag))
        }
        
        var asset: PHAsset? = nil
        if assets.count > 0 {
            asset = assets[atPath]
        }
        
        if let creationDate = asset?.creationDate {
            self.timeStamp = NSDateFormatter.localizedStringFromDate(creationDate,
                                                                           dateStyle: .MediumStyle,
                                                                           timeStyle: .MediumStyle
            )
        } else {
            timeStamp = nil
        }
        
        return [manager, asset]
    }
    
    func setAsset(manager: PHImageManager, asset: PHAsset) {
        // get mediaview
        self.tag = Int(manager.requestImageForAsset(asset,
            targetSize: CGSize(width: 100.0, height: 100.0),
            contentMode: .AspectFill,
        options: nil) { (result, _) in
            let tag = self.viewInQueue!.tag
            switch(tag) {
            case DataTag.Image.rawValue:
                let imageView = self.viewInQueue as! UIImageView
                imageView.image = result
            default: break
            }
        })
    }
    
    func setMediaWithAsset(inout assets: [PHAsset], path: Int) {
        let values = self.pullAsset(assets, atPath: path)
        let manager = values[0] as? PHImageManager
        let asset = values[1] as? PHAsset
        self.setAsset(manager!, asset: asset!)
    }
    
    func setMedia(assets: [PHAsset], path: Int) {
        let values = self.pullAsset(assets, atPath: path)
        let manager = values[0] as? PHImageManager
        let asset = values[1] as? PHAsset
        self.setAsset(manager!, asset: asset!)
    }
    
    @nonobjc func setMedia(cache: NSMutableDictionary, path: Int) {
        let text = cache.objectForKey(path) as! String
        let textView = viewInQueue as! UITextView
        textView.text = text
        textViewFitToContent(textView)
        textView.layoutIfNeeded()
    }
    
    func compileView(withAsset assets: [PHAsset]?, withCache cache: NSMutableDictionary?, containsMessage message: String?, atPath path: Int, asType type: DataType) {
        switch type {
        case .Text:
            setTextView(message!, key: path, cache: cache!)
        case .Image:
            setAsImageView()
        case .Video: break
        }
        
        if type == .Text {
            setMedia(cache!, path: path)
        }
        else {
            setMedia(assets!, path: path)
        }
    }
    
    func compileViewWithAsset(inout withAsset assets: [PHAsset], atPath path: Int, asType type: DataType) {
        switch type {
        case .Image:
            setAsImageView()
        case .Video: break
        default: break
        }
        imageManager!.saveAsset(&assets)
        setMediaWithAsset(&assets, path: path)
        addViewToContainer()
    }
}
