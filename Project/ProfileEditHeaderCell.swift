//
//  ProfileEditHeaderCell.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 3/8/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

class ProfileEditHeaderCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // Resign First Responder
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.contentView.endEditing(true)
    }
}
