//
//  SignUpView.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 2/24/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit

class SignUpView: UIViewController, FBSDKLoginButtonDelegate {
    
    
    @IBOutlet var fbRegister: FBSDKLoginButton!
    @IBOutlet weak var createAccountButton: UIButton!
    
    var pageID: [Dictionary<String, String>]!
    var list = [String]()
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        // Set Button
        let averageHeight = createAccountButton.frame.size.width
        let buttonRadius: CGFloat = averageHeight / 2
        createAccountButton.layer.cornerRadius = buttonRadius
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureFacebook()
    }
    
    func configureFacebook() {
        fbRegister.readPermissions = ["public_profile", "email", "user_relationship_details",
            "user_religion_politics", "user_hometown", "user_relationship_details",
            "user_website", "user_education_history", "user_work_history",
            "user_birthday", "user_actions.news", "user_actions.books",
            "user_actions.fitness", "user_actions.music", "user_actions.video",
            "user_likes", "user_managed_groups"];
        fbRegister.delegate = self
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        returnUserData({ (result) -> Void in
            var data = result
            self.returnUserLikes({ (result) -> Void in
                self.pageID = result
                self.getUserLikes({ (result) -> Void in
                    let i = data["interests"]!.count
                    if (result.count + i) > 0 {
                        var interests = data["interests"] as! [String]
                        for interest in result {
                            interests.append(interest)
                        }
                        data["interests"] = Array(Set(interests))
                        self.list.removeAll()
                    }
                    self.returnUserGroups({ (result) -> Void in
                        self.pageID = result
                        self.getUserGroups({ (result) -> Void in
                            let j = data["organizations"]!.count
                            if (result.count + j) > 0 {
                                var organizations = data["organizations"] as! [String]
                                for organization in result {
                                    organizations.append(organization)
                                }
                                data["organizations"] = Array(Set(organizations))
                            }
                            
                            data["usertype"] = "new"
                            
                            user.save(data)
                            user.hidden.isAdvertising = true
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                let viewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Root") as! RootViewController
                                self.presentViewController(viewController, animated: false, completion: nil)
                            })
                        })
                    })
                })
            })
        })
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
    
    
    // MARK: - Facebook Graph Request
    
    // Likes
    
    func returnUserLikes(completion:(result: [Dictionary<String, String>]) -> Void) {
        let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "\(user.hidden.facebookID)/likes", parameters: ["fields": "data"])
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            if error == nil {
                if let pageData = result.objectForKey("data") as? [Dictionary<String, String>] { completion(result: pageData) }
            }
            else {
                print("\(error)")
            }
        })
    }
    
    func getUserLikes(completion:(result: [String]) -> Void) {
        if pageID.count > 0 {
            for data in self.pageID {
                if let id = data["id"] {
                    let getPage = FBSDKGraphRequest(graphPath: "\(id)", parameters: ["fields" : "name"], HTTPMethod: "GET")
                    getPage.startWithCompletionHandler({ (connection, result, error) -> Void in
                        if error == nil {
                            let interest = result.objectForKey("name") as! String
                            self.list.append(interest)
                            if self.list.count == self.pageID.count { completion(result: self.list) }
                        }
                        else {
                            print("\(error)")
                        }
                    })
                }
            }
        }
        else { completion(result: self.list) }
    }
    
    // Groups
    
    func returnUserGroups(completion:(result: [Dictionary<String, String>]) -> Void) {
        let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "\(user.hidden.facebookID)/groups", parameters: ["fields": "data"])
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            if error == nil {
                if let pageData = result.objectForKey("data") as? [Dictionary<String, String>] { completion(result: pageData) }
            }
            else {
                print("\(error)")
            }
        })
    }
    
    func getUserGroups(completion:(result: [String]) -> Void) {
        if pageID.count > 0 {
            for data in self.pageID {
                if let id = data["id"] {
                    let getPage = FBSDKGraphRequest(graphPath: "\(id)", parameters: ["fields" : "name"], HTTPMethod: "GET")
                    getPage.startWithCompletionHandler({ (connection, result, error) -> Void in
                        if error == nil {
                            let organization = result.objectForKey("name") as! String
                            self.list.append(organization)
                            if self.list.count == self.pageID.count { completion(result: self.list) }
                        }
                        else {
                            print("\(error)")
                        }
                    })
                }
            }
        }
        else { completion(result: self.list) }
    }
    
    // Main
    
    func returnUserData(completion:(result: [String : AnyObject]) -> Void) {
        let fields = "first_name, last_name, middle_name, name_format, birthday, context, political, religion, website, email, work, education, verified, is_verified, picture.type(large), gender, interested_in, is_shared_login, sports, hometown, languages, favorite_teams, inspirational_people, likes, groups"
        let parameter = ["fields" : fields]
        
        var name: [String]!
        var email: String!
        var image: UIImage!
        var interests = Set<String>()
        var organizations = Set<String>()
        
        FBSDKGraphRequest.init(graphPath: "me", parameters: parameter).startWithCompletionHandler { (connection, result, error) -> Void in
            if error == nil {
                // Determine if FB account is verified
                let verified = result.objectForKey("verified") as! Bool
                let is_verified = result.objectForKey("is_verified") as! Bool
                let is_shared_login = result.objectForKey("is_shared_login") as! Bool
                
                if (verified == true || is_verified == true) && is_shared_login == false {
                    let firstName = result.objectForKey("first_name") as! String
                    let lastName = result.objectForKey("last_name") as! String
                    name = [firstName, lastName] as [String]
                    email = result.objectForKey("email") as! String
                    
                    user.hidden.context = result.objectForKey("context")!
                    user.hidden.facebookID = result.objectForKey("id") as! String
                    if let middle_name = result.objectForKey("middle_name") as? String { user.hidden.middleName = middle_name }
                    if let name_format = result.objectForKey("name_format") as? String { user.hidden.name_format = name_format }
                    if let birthday = result.objectForKey("birthday") as? String { user.hidden.birthday = birthday }
                    if let gender = result.objectForKey("gender") as? String { user.hidden.gender = gender }
                    if let political = result.objectForKey("political") as? String { user.hidden.political = political }
                    if let religion = result.objectForKey("religion") as? String { user.hidden.religion = religion }
                    if let interested_in = result.objectForKey("interested_in") as? [String] { user.hidden.interested_in = interested_in }
                    if let website = result.objectForKey("website") as? String { user.hidden.website = website }
                    if let hometown = result.objectForKey("hometown") as? String { user.hidden.hometown = hometown }
                    
                    
                    if let strPictureURL: String = result.objectForKey("picture")?.objectForKey("data")?.objectForKey("url") as? String {
                        let profileImage = UIImage(data: NSData(contentsOfURL: NSURL(string: strPictureURL)!)!)!
                        image = profileImage
                    }
                    if let work = result.objectForKey("work") as? [Dictionary<String, AnyObject>] {
                        if let profession = work.first!["employer"]!.valueForKey("name") as? String { user.hidden.profession = profession }
                    }
                    if let education = result.objectForKey("education") as? [Dictionary<String, AnyObject>] {
                        if let highschool = education.first!["school"]!.valueForKey("name") as? String { user.hidden.highschool.append(highschool) }
                        if let major = education.last!["concentration"]!.valueForKey("name") as? String { user.hidden.major = major }
                        if let college = education.last!["school"]!.valueForKey("name") as? String {
                            user.hidden.college.append(college)
                            organizations.insert(college)
                        }
                    }
                    if let languages = result.objectForKey("languages") as? [Dictionary<String, AnyObject>] {
                        for language in languages {
                            user.hidden.languages.append(language["name"] as! String)
                        }
                    }
                    if let sports = result.objectForKey("sports") as? [Dictionary<String, AnyObject>] {
                        print(sports)
                    }
                    if let favorite_teams = result.objectForKey("favorite_teams") as? [Dictionary<String, AnyObject>] {
                        for interest in favorite_teams {
                            interests.insert(interest["name"] as! String)
                        }
                    }
                    if let inspirationalPeople = result.objectForKey("inspirational_people") as? [Dictionary<String, AnyObject>] {
                        print(inspirationalPeople)
                    }
                    if let meetingFor = result.objectForKey("meeting_for") as? [Dictionary<String, String>] {
                        print(meetingFor)
                    }
                    
                    let data: [String : AnyObject] = ["name" : name, "email" : email, "organizations" : Array(organizations), "interests" : Array(interests), "image" : [image, false]]
                    completion(result: data)
                }
                else {
                    print("User is not a verified individual")
                }
            }
            else {
                print("\(error)")
            }
        }
    }
}

