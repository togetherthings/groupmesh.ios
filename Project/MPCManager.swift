//
//  MPCManager.swift
//  GroupMesh
//  Created by Leonel Da Costa on 7/22/16.
//  Copyright (c) TogetherThings. All rights reserved.

import UIKit
import MultipeerConnectivity
import CoreData
import ObjectiveC
import Foundation


protocol MPCManagerDelegate {
    func foundPeer()
    
    func lostPeer()
    
    func automaticInvitation()
    
    func meshInvitation(fromPeer: String, group: MeshGroup)
    
    func sendDisplayImage()
}


class MPCManager: NSObject, MCSessionDelegate, MCNearbyServiceBrowserDelegate, MCNearbyServiceAdvertiserDelegate {

    var delegate: MPCManagerDelegate?
    
    var session: MCSession!
    
    var publicSessions = [MeshGroup]()
    
    var peer: MCPeerID!
    
    var browser: MCNearbyServiceBrowser!
    
    var advertiser: MCNearbyServiceAdvertiser!
    
    var foundPeers = [MCPeerID]()
    
    var invitationHandler: ((Bool, MCSession)->Void)!
    
    
    override init() {
        super.init()
        // Set advertiser info
        setAdvertiser()
        
        createPublicSession()
        session = publicSessions.last?.getSession()
        
        browser = MCNearbyServiceBrowser(peer: peer, serviceType: "chatbox-mpc")
        browser.delegate = self
        
        advertiser = MCNearbyServiceAdvertiser(peer: peer, discoveryInfo: nil, serviceType: "chatbox-mpc")
        advertiser.delegate = self
    }
    
    
    func createPublicSession() {
        let session = MCSession(peer: peer)
        session.delegate = self
        let mesh = MeshGroup()
        mesh.addSession(session)
        publicSessions.append(mesh)
    }
    
    func setAdvertiser() {
        // Set peer name
        peer = MCPeerID(displayName: user.getFullName())
        peer.displayImage = user.adImage
    }
    
    
    // MARK: MCNearbyServiceBrowserDelegate method implementation
    
    func browser(browser: MCNearbyServiceBrowser, foundPeer peer: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        let data = ["default"]
        let methodData = NSKeyedArchiver.archivedDataWithRootObject(data)
        let mesh = publicSessions.last
        
        // Network
        if publicSessions.last!.getLength() < 8 {
            mesh?.addMember(peer)
        }
        else {
            createPublicSession()
            publicSessions.last?.addMember(peer)
            session = publicSessions.last?.getSession()
        }
        
        print(peer)
        browser.invitePeer(peer, toSession: session, withContext: methodData, timeout: 20)
    }
    
    
    func browser(browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        for (index, aPeer) in foundPeers.enumerate(){
            if aPeer == peerID {
                foundPeers.removeAtIndex(index)
                break
            }
        }
        delegate?.lostPeer()
    }
    
    
    func browser(browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: NSError) {
        print(error.localizedDescription)
    }
    
    
    // MARK: MCNearbyServiceAdvertiserDelegate method implementation
    
    func advertiser(advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: NSData?, invitationHandler: ((Bool, MCSession) -> Void)) {
        self.invitationHandler = invitationHandler
        if let data = context {
            let unarchived = NSKeyedUnarchiver.unarchiveObjectWithData(data)
            print(unarchived)
            let dataArray = unarchived! as! [AnyObject]
            switch(dataArray[0] as! String) {
                // 0th array value is the method name
                case "sendCurrentMesh":
                    let groupMesh = MeshGroup(name: dataArray[1] as! String)
                    groupMesh.setGroupMesh(dataArray[2] as! [AnyObject])
                    delegate?.meshInvitation(peerID.displayName, group: groupMesh)
                case "chat":
                    connectedWithPeer(peer)
                default:
                    delegate?.automaticInvitation()
            }
        }
        else {
            NSNotificationCenter.defaultCenter().postNotificationName("invitationWasReceived", object: peerID)
        }
    }
    
    
    func advertiser(advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: NSError) {
        print(error.localizedDescription)
    }
    
    
    // MARK: MCSessionDelegate method implementation
    
    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState) {
        switch state {
            case MCSessionState.Connected:
                print("Connected to session: \(session)")
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.foundPeers.append(peerID)
                    self.sendPublicImage(user.adImage)
                })
            
            case MCSessionState.Connecting:
                print("Connecting to session: \(session)")
            
            default:
                print("Did not connect to session: \(session)")
        }
    }
    
    
    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID) {
        let unarchived = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! [String: AnyObject]
        let action = Array(unarchived.keys)
        switch (action[0]) {
            case "image":
                let image = unarchived["image"] as! UIImage
                let data = ["fromPeer" : peerID, "image" : image]
                NSNotificationCenter.defaultCenter().postNotificationName("receivedImageUpdateNotification", object: data)
            default:
                let dictionary: [String: AnyObject] = ["data": data, "fromPeer": peerID]
                NSNotificationCenter.defaultCenter().postNotificationName("receivedMPCDataNotification", object: dictionary)
        }
    }
    
    
    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress) { }
    
    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?) { }
    
    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID) { }
    
    
    
    // MARK: Custom method implementation
    
    func connectedWithPeer(peer: MCPeerID) {
        NSNotificationCenter.defaultCenter().postNotificationName("connectedWithPeer", object: peer)
    }
    
    func sendData(dictionaryWithData dictionary: Dictionary<String, AnyObject>, toPeer targetPeer: MCPeerID) -> Bool {
        let dataToSend = NSKeyedArchiver.archivedDataWithRootObject(dictionary)
        let peersArray = NSArray(object: targetPeer)
        var error: NSError?
        
        do {
            try session.sendData(dataToSend, toPeers: peersArray as! [MCPeerID], withMode: MCSessionSendDataMode.Reliable)
        } catch let error1 as NSError {
            error = error1
            print(error?.localizedDescription)
            return false
        }
        
        return true
    }
    
    func sendPublicImage(image: UIImage) {
        let action = ["image": image]
        let data = NSKeyedArchiver.archivedDataWithRootObject(action)
        let peersArray = NSArray(array: session.connectedPeers)
        var error: NSError?
        do {
            try session.sendData(data, toPeers: peersArray as! [MCPeerID], withMode: MCSessionSendDataMode.Reliable)
        } catch let error1 as NSError {
            error = error1
            print(error?.localizedDescription)
        }
    }
    
    func setAdvertiserImage(image: UIImage, fromPeer: MCPeerID) {
        let array = session.connectedPeers
        if let peerInSession = array.indexOf(fromPeer) {
            array[peerInSession].displayImage = image
            print("setting image from \(array[peerInSession])")
            print(array[peerInSession].displayImage)
        }
        delegate?.foundPeer()
    }
    
    
}

// MCPeerID Stored Property Extension

extension MCPeerID {
    
    private struct CustomProperty {
        static var displayImage: UIImage? = nil
    }
    
    var displayImage: UIImage? {
        get {
            return objc_getAssociatedObject(self, &CustomProperty.displayImage) as? UIImage
        }
        set {
            if let unwrappedValue = newValue {
                objc_setAssociatedObject(self, &CustomProperty.displayImage, unwrappedValue as UIImage?, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
}



