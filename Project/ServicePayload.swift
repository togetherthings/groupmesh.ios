//
//  ServicePayload.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 7/23/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import Foundation

class ServicePayload: NSObject {
    
    var loaded: NSDictionary!
    
    init(data: NSDictionary) {
        loaded = data
    }
    
    func commit() {
        if let token = loaded["token"] as? String {
            user.hidden.token = NSString(string: token)
        }
    }
}