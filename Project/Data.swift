//
//  Data.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 3/2/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit


// MARK: - Property list conversion protocol

protocol PropertyListReadable {
    init?(propertyListRepresentation:NSDictionary?)
    func propertyListRepresentation() -> NSDictionary
}

func extractValuesFromPropertyListArray<T:PropertyListReadable>(propertyListArray:[AnyObject]?) -> [T] {
    guard let encodedArray = propertyListArray else {return []}
    return encodedArray.map{$0 as? NSDictionary}.flatMap{T(propertyListRepresentation:$0)}
}

func saveValuesToDefaults<T:PropertyListReadable>(newValues:[T], key:String) {
    let encodedValues = newValues.map{$0.propertyListRepresentation()}
    NSUserDefaults.standardUserDefaults().setObject(encodedValues, forKey:key)
}

// MARK: - Data enums

enum DataAction {
    case Read
    case Write
}

enum DataType: String {
    case Text = "text"
    case Image = "image"
    case Video = "video"
}

enum DataTag: Int {
    case Text = 101
    case Image = 202
    case Video = 303
}

////////////////////

class Data: NSObject {
    
    let title: String!
    var data: [String : AnyObject]?
    var text: String?
    
    init(title: String) {
        self.title = title
    }
    
    init(title: String, text: String) {
        self.title = title
        self.text = text
    }
    
    init(title: String, data: [String : AnyObject]) {
        self.title = title
        self.data = data
    }
    
    init(title: String, text: String, data: [String : AnyObject]) {
        self.title = title
        self.text = text
        self.data = data
    }

}
