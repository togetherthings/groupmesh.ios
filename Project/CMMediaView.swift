//
//  MediaView.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 6/22/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit

class CMMediaView: UIView {
    
    var heightConstraint: NSLayoutConstraint!
    
    var defaultMediaHeight: CGFloat!
    var superViewHeightConstraint: NSLayoutConstraint?
    
    var mainViewHeight: CGFloat = 179
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.layer.borderWidth = 1
        self.layer.cornerRadius = CMConstant.cornerRadius
        self.layer.borderColor = CMColor.lightGray.CGColor
        
        defaultMediaHeight = self.frame.height * 3.0
    }
    
    func setOuterViewHeightConstraint(constraint: NSLayoutConstraint) {
        superViewHeightConstraint = constraint
    }
    
    func hasSuperViewHeightConstraint() -> Bool {
        if let _ = superViewHeightConstraint {
            return true
        }
        
        return false
    }
    
    func adjustedTextView() {
        if let textView = self.viewWithTag(DataTag.Text.rawValue) as? CMTextView {
            let topConstraint = self.constraints[2]
            let numberOfLines = textView.textContainer.layoutManager?.numberOfLines(textView)
            print(numberOfLines)
            topConstraint.constant = numberOfLines?.height > 1 ? 0 : 10
            self.layoutIfNeeded()
        }
    }
    
    func toggelReadWrite() {
        if let textView = self.viewWithTag(DataTag.Text.rawValue) as? CMTextView {
            textView.userInteractionEnabled = !textView.userInteractionEnabled
        }
    }
    
    func clear() {
        self.removeConstraints(self.constraints)
        self.subviews.forEach({ $0.removeFromSuperview() })
    }
    
    func updateLayout() {
        self.layoutSubviews()
        self.layoutIfNeeded()
    }
    
    private func createTextView() -> CMTextView {
        let rect = CGRect(x: 0, y: 0, width: self.bounds.width, height: 0)
        let textView = CMTextView(frame: rect)
        textView.layer.borderWidth = 1
        textView.layer.borderColor = CMColor.lightGray.CGColor
        return textView
    }
    
    private func createImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .ScaleAspectFit
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = CMConstant.cornerRadius
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = CMColor.coldBlue.CGColor
        imageView.tag = DataTag.Image.rawValue
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    private func createVideoView() -> UIView {
        return UIView()
    }
}
