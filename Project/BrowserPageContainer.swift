//
//  BrowserPageContainer.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 3/25/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

class BrowserPageContainer: UIViewController {
    
    @IBOutlet weak var searchButton: UIButton!
    
    var searchBar: UISearchBar!
    var leftBarButton: UIBarButtonItem!
    var rightBarButtons: [UIBarButtonItem]!
    
    var segmentedControl = HMSegmentedControl(sectionTitles: ["People", "Events"])
    private(set) lazy var orderedViewControllers: [UIViewController] = { return [PageViewController.newActivityViewController("People"), PageViewController.newActivityViewController("Event")] }()
    var pageViewController: PageViewController? {
        didSet {
            pageViewController?.pageDelegate = self
            pageViewController?.orderedViewControllers = self.orderedViewControllers
        }
    }
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let color = UIColor(red: 108.0/255.0, green: 151.0/255.0, blue: 202.0/255.0, alpha: 1.0)
        
        // Segmented Control
        
        segmentedControl.autoresizingMask = [.FlexibleRightMargin, .FlexibleWidth]
        segmentedControl.frame = CGRectMake(0, 65, view.frame.width, 40)
        segmentedControl.segmentEdgeInset = UIEdgeInsetsMake(0, 10, 0, 10)
        segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe
        segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown
        segmentedControl.selectionIndicatorColor = color
        segmentedControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName : color]
        
        // segmentedControl: Format
        let titleFormatterBlock: HMTitleFormatterBlock = { (control: AnyObject!, title: String!, index: UInt, selected: Bool) -> NSAttributedString in
            let textFont = UIFont(name: "Helvetica", size: 15)!
            let color = UIColor(red: 130.0/255.0, green: 130.0/255.0, blue: 130.0/255.0, alpha: 1.0)
            let attributes: [String : AnyObject] = [NSForegroundColorAttributeName : color, NSFontAttributeName : textFont]
            let attributedString = NSAttributedString(string: title, attributes: attributes)
            return attributedString
        }
        segmentedControl.titleFormatter = titleFormatterBlock
        
        segmentedControl.addTarget(self, action: #selector(BrowserPageContainer.didChangePageControlValue), forControlEvents: .ValueChanged)
        
        self.view.addSubview(segmentedControl)
        
        
        // UISearchBar
        
        searchBar = UISearchBar()
        searchBar.searchBarStyle = .Minimal
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        
        if #available(iOS 9.0, *) {
            (UIBarButtonItem.appearanceWhenContainedInInstancesOfClasses([UISearchBar.self])).tintColor = color
        } else {
            searchBar.tintColor = color
        }
        
        // UIBarButtonItems
        
        leftBarButton = navigationItem.leftBarButtonItem
        rightBarButtons = navigationItem.rightBarButtonItems
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let pageViewController = segue.destinationViewController as? PageViewController {
            self.pageViewController = pageViewController
        }
    }
    
    func didChangePageControlValue() {
        pageViewController?.scrollToViewController(index: segmentedControl.selectedSegmentIndex)
    }
    
    @IBAction func searchButtonPressed(sender: AnyObject) {
        searchBar.alpha = 0.0
        navigationItem.titleView = searchBar
        navigationItem.setLeftBarButtonItem(nil, animated: true)
        navigationItem.setRightBarButtonItems(nil, animated: false)
        UIView.animateWithDuration(0.5,
            animations: {
                self.searchBar.alpha = 1.0
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
    }
    
    @IBAction func startStopAdvertising(sender: AnyObject) {
        let actionSheet = UIAlertController(title: "", message: "Change Visibility", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        var actionTitle: String
        if user.hidden.isAdvertising == true { actionTitle = "Make me invisible to others" }
        else { actionTitle = "Make me visible to others" }
        
        let visibilityAction: UIAlertAction = UIAlertAction(title: actionTitle, style: UIAlertActionStyle.Default) { (alertAction) -> Void in
            if user.hidden.isAdvertising == true { self.appDelegate.mpcManager.advertiser.stopAdvertisingPeer() }
            else { self.appDelegate.mpcManager.advertiser.startAdvertisingPeer() }
            user.hidden.isAdvertising = !user.hidden.isAdvertising
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (alertAction) -> Void in }
        
        actionSheet.addAction(visibilityAction)
        actionSheet.addAction(cancelAction)
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    
    @IBAction func pressedMesh(sender: AnyObject) {
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
}

extension BrowserPageContainer: PageViewControllerDelegate {
    
    func pageViewController(pageViewController: PageViewController, didUpdatePageCount count: Int) {
        segmentedControl.selectedSegmentIndex = 0
    }
    
    func pageViewController(pageViewController: PageViewController, didUpdatePageIndex index: Int) {
        segmentedControl.setSelectedSegmentIndex(UInt(index), animated: true)
    }
}

extension BrowserPageContainer: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        UIView.animateWithDuration(0.1,
            animations: { () -> Void in
                searchBar.alpha = 0.0
            },
            completion: { (finished) -> Void in
                self.navigationItem.titleView = nil
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.navigationItem.leftBarButtonItem = self.leftBarButton
                    self.navigationItem.rightBarButtonItems = self.rightBarButtons
            })
        })
    }
}
