//
//  LMPopoverView.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 5/18/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class LMPopoverView: ActivityTemplateController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate {
    
    let MEDIA_VIEW_X: CGFloat = 13
    let MEDIA_VIEW_Y: CGFloat = 10
    let MEDIA_VIEW_WIDTH: CGFloat = 350
    let MEDIA_VIEW_HEIGHT: CGFloat = 350
    
    let IMAGE_TAG = 101
    let VIDEO_TAG = 202
    
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    
    let offset_HeaderStop:CGFloat = 5.0 // At this offset the Header stops its transformations
    let distance_W_LabelHeader:CGFloat = 25.0 // The distance between bottom of header and header label
    
    var message: [LocalMessage]!
    
    var cellImageView: UIImageView!
    var cellVideoView: UIView!
    
    var postedImageCount = 0
    var postedVideoCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Set image UI
        imageView.layer.cornerRadius =  (imageView.frame.height / 2)
        imageView.contentMode = .ScaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        headerImage.layer.cornerRadius =  (headerImage.frame.height / 2)
        headerImage.contentMode = .ScaleAspectFill
        headerImage.layer.masksToBounds = true
        headerImage.clipsToBounds = true
        headerImage.layer.borderWidth = 0.5
        headerImage.layer.borderColor = UIColor.lightGrayColor().CGColor
        headerImage.alpha = 0.0
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 176
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: UITableView related method implementation
    // Note: Consider implementation of simple cell height calculator
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return message.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let localMessage = message[indexPath.row]
        let dataType = localMessage.mediaType!
        let cell = tableView.dequeueReusableCellWithIdentifier("popOTMessage") as! MessageTableCell
        
        switch dataType {
        case .Text:
            cell.setView(atPath: indexPath.row, withMessage: localMessage, inAssets: nil, inCache: textCache)
        case .Image:
            cell.setView(atPath: postedImageCount, withMessage: localMessage, inAssets: imageAssets, inCache: nil)
            postedImageCount += 1
        case .Video:
            cell.setView(atPath: postedVideoCount, withMessage: localMessage, inAssets: videoAssets, inCache: nil)
            postedVideoCount += 1
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        LMPopover()
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    // Receive Data
    
    func receiveLocalMessages(data: [AnyObject]) {
        message = (data as! [LocalMessage]).reverse()
    }
    
    func receiveAssets(assetsList: [String : [PHAsset]]) {
        if let images = assetsList["images"] {
            self.imageAssets = images
        }
        if let videos = assetsList["videos"] {
            self.videoAssets = videos
        }
    }
    
    // Popover
    
    func LMPopover() {
        self.performSegueWithIdentifier("showLMActivity", sender: self)
    }
    
    // Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showLMActivity" {
            let vc = segue.destinationViewController as! LMPopoverActivity
            if let controller = vc.popoverPresentationController {
                controller.delegate = self
                let _ = vc.view
            }
        }
    }
    
    
    // MARK: - Popover UI
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        
        var avatarTransform = CATransform3DIdentity
        var headerTransform = CATransform3DIdentity
        
        // PULL DOWN
        
        if offset < 0 {
            
            let headerScaleFactor:CGFloat = -(offset) / header.bounds.height
            let headerSizevariation = ((header.bounds.height * (1.0 + headerScaleFactor)) - header.bounds.height) / 2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            
            header.layer.transform = headerTransform
            header.layer.zPosition = 0
            headerImage.hidden = true
            headerImage.alpha = 0.0
        }
            
            // SCROLL UP/DOWN
            
        else {
            
            // Header
            
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
            
            // Header Items
            
            headerImage.hidden = false
            let alignToNameLabel = -offset + nameLabel.frame.origin.y + header.frame.height - (offset_HeaderStop * 3)
            
            headerImage.frame.origin = CGPointMake(headerImage.frame.origin.x, max(alignToNameLabel, distance_W_LabelHeader + offset_HeaderStop))
            
            if offset > 95 {
                headerImage.alpha = 1.0
            }
            else {
                headerImage.alpha = CGFloat(offset / 95.0)
            }
            
            closeButton.layer.zPosition = 3
            
            // Avatar
            
            let avatarScaleFactor = (min(offset_HeaderStop, offset)) / imageView.bounds.height / 0.6
            let avatarSizeVariation = ((imageView.bounds.height * (1.0 + avatarScaleFactor)) - imageView.bounds.height) / 2.0
            avatarTransform = CATransform3DTranslate(avatarTransform, 0, avatarSizeVariation, 0)
            avatarTransform = CATransform3DScale(avatarTransform, 1.0 - avatarScaleFactor, 1.0 - avatarScaleFactor, 0)
            
            if offset <= offset_HeaderStop {
                
                if imageView.layer.zPosition < header.layer.zPosition {
                    header.layer.zPosition = 0
                }
                
            }
            else {
                if imageView.layer.zPosition >= header.layer.zPosition {
                    header.layer.zPosition = 2
                }
            }
        }
        
        // Apply Transformations
        
        header.layer.transform = headerTransform
        imageView.layer.transform = avatarTransform
    }
}
