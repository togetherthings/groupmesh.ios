//
//  MessageTableCell.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 5/28/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit
import Photos

class MessageTableCell: CustomTableViewCell {
    
    
    @IBOutlet weak var mediaViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaView: UIView!
    
    var isSet = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        createMessageContainer()
        setMediaContainer(mediaView)
    }
    
    func setView(atPath path: Int, withMessage: LocalMessage, inAssets assets: [PHAsset]?, inCache cache: NSMutableDictionary?) {
        let message = withMessage.message
        let dataType = withMessage.mediaType!
        
        if !isSet {
            if dataType == .Text {
                let text = message as! String
                compileView(withAsset: nil, withCache: cache, containsMessage: text, atPath: path, asType: dataType)
            }
            else {
                compileView(withAsset: assets, withCache: nil, containsMessage: nil, atPath: path, asType: dataType)
            }
            isSet = true
        }
        else {
            if dataType == .Text {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.setMedia(cache!, path: path)
                })
            }
            else {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.setMedia(assets!, path: path)
                })
            }
        }
        /*case .Video: break
            let videoPlayer = VideoPlayer()
            if let incompleteCell = cell {
                incompleteCell.textLabel?.hidden = true
                //incompleteCell.mediaView.addSubview(videoPlayer.createVideoView(x!, y: y!, width: width!, height: height! * 1.40))
            }
            
            var videoURL = videoCache.objectForKey(indexPath.row)
            
            if videoURL == nil {
                videoURL = message[indexPath.row].message[1] as! NSURL
                videoCache.setObject(videoURL!, forKey: indexPath.row)
            }
            
            let playerItem = AVPlayerItem(URL: videoURL as! NSURL)
            videoPlayer.player.replaceCurrentItemWithPlayerItem(playerItem)
            videoPlayer.loadingIndicatorView.startAnimating()
            videoPlayer.player.pause()*/
    }
}
