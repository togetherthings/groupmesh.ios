//
//  Archiver.swift
//  GroupMesh
//
//  Created by Leo Da Costa on 7/23/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import Foundation

class Archiver: NSObject {
    
    private func UserArchiver() {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver.init(forWritingWithMutableData: data)
        archiver.encodeObject(user, forKey: "user_public"); archiver.finishEncoding()
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).first!
        let path = documentsPath.stringByAppendingString("/user.archive")
        let archiveURL = NSURL(fileURLWithPath: path)
        
        do {
            try data.writeToURL(archiveURL, options: .DataWritingAtomic)
            print("archived")
        } catch let error as NSError {
            print("\(error)")
        }
    }
    
    private func UserUnarchiver() -> User {
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).first!
        let path = documentsPath.stringByAppendingString("/user.archive")
        let archiveURL = NSURL(fileURLWithPath: path)
        let data = NSData(contentsOfURL: archiveURL)
        
        let unarchiver = NSKeyedUnarchiver(forReadingWithData: data!)
        let userUnarchived = unarchiver.decodeObjectForKey("user_public") as! User; unarchiver.finishDecoding()
        
        return userUnarchived
    }
}