//
//  Bridging-Header.h
//  ChatBox
//
//  Created by Leo Da Costa on 2/24/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#ifndef Bridging_Header_h
#define Bridging_Header_h

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <JSQMessagesViewController/JSQMessages.h>
#import <HMSegmentedControl.h>


@interface ObjectiveCFile : UIViewController
- (void)displayMessageFromCreatedObjectiveCFile;
@end

#endif /* Bridging_Header_h */
