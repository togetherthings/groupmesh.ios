//
//  DefaultImage.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 1/11/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DefaultImage: NSObject {
    
    var initials: NSString!
    var image: UIImage!
    
    init(image: UIImage, firstName: String, lastName: String) {
        self.image = image
        let firstInitial = firstName.substringToIndex(firstName.startIndex.advancedBy(1))
        let lastInitial = lastName.substringToIndex(firstName.startIndex.advancedBy(1))
        initials = NSString(string: firstInitial + lastInitial)
    }
    
    func makeImageAs(image: UIImage) {
        self.image = image
    }
    
    func setTextOnImage(color: UIColor, atPoint: CGPoint, withTextSize: CGFloat) -> UIImage {
        return ImageManager.setTextOnImage(initials, image: image, color: color, atPoint: atPoint, withTextSize: withTextSize)
    }
    
    func setTextOnImage(color: UIColor, withTextSize: CGFloat) -> UIImage {
        return ImageManager.setTextOnImage(initials, image: image, color: color, withTextSize: withTextSize)
    }
    
    static func makeUserDefaultImage(name: [String]) -> UIImage {
        let image = ImageManager.resizeImage(UIImage(named: "Default.png")!, toTheSize: CGSizeMake(200, 200))
        let defaultImage = DefaultImage(image: image, firstName: name[0], lastName: name[1])
        let displayImage = defaultImage.setTextOnImage(UIColor.whiteColor(), withTextSize: 80)
        
        return displayImage
    }
}
