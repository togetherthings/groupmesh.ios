//
//  OTView.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 5/20/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit
import MobileCoreServices
import MediaPlayer

class OTView: UIViewController, UITextViewDelegate {
    
    let CHARACTERS_MAX = 200
    let OT_SIZE = CGSizeMake(320, 250)
    
    var TRANSITION_VIEW_HEIGHT: CGFloat!
    
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var mediaView: UIView!
    @IBOutlet weak var charCounter: UILabel!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    var currentView: DataType!
    var imageView: UIImageView!
    var videoView: UIView!
    var moviePlayer: MPMoviePlayerController!
    
    var imageManager: ImageManager?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OTView.mediaFixture(_:)), name: "receivedCallData", object: nil)
        
        innerView.layer.cornerRadius = CMConstant.cornerRadius
        mediaView.layer.cornerRadius = CMConstant.cornerRadius
        textView.delegate = self
        currentView = .Text
        
        // Image View
        imageView = UIImageView(frame: CGRectZero)
        imageView.layer.cornerRadius = CMConstant.cornerRadius
        imageView.contentMode = .ScaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
        imageView.tag = DataTag.Image.rawValue
        imageView.backgroundColor = CMColor.coldBlue
        
        videoView = UIView(frame: CGRectMake(innerView.frame.origin.x, innerView.frame.origin.y, innerView.frame.width, 250))
        videoView.layer.cornerRadius = CMConstant.cornerRadius
        
        TRANSITION_VIEW_HEIGHT = OT_SIZE.height + OT_SIZE.height * 0.6
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        charCounter.text = "\(CHARACTERS_MAX)"
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        let parent = self.popoverPresentationController?.presentingViewController.childViewControllers[0].childViewControllers[0].childViewControllers[0].childViewControllers[0]
        parent!.view.viewWithTag(123)?.removeFromSuperview()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Button Outlets
    
    @IBAction func videoAction(sender: AnyObject) {
        let call = "videoCall"
        self.dismissViewControllerAnimated(true, completion: nil)
        NSNotificationCenter.defaultCenter().postNotificationName("openThought-calls", object: call)
    }
    
    @IBAction func cameraAction(sender: AnyObject) {
        clearMediaView()
        transitionToImageSelection()
    }
    
    @IBAction func confirmAction(sender: AnyObject) {
        var sendData = [String : AnyObject]()
        
        if (textView.text != nil && textView.text.characters.count > 0) || mediaView.subviews.count > 0 {
            sendData["author"] = user.getFullName(); sendData["displayImage"] = user.adImage
            
            if let text = textView.text {
                sendData["message"] = text
            }
            
            if mediaView.subviews.count > 0 {
                let media = mediaView.subviews[0]
                let dataTag = DataTag(rawValue: media.tag)
                switch dataTag! {
                case .Image:
                    sendData["mediaType"] = DataType.Image.rawValue
                case .Video:
                    sendData["mediaType"] = DataType.Video.rawValue
                default: break
                }
                sendData["media"] = NSArray(array: [media])
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                NSNotificationCenter.defaultCenter().postNotificationName("receivedLocalMessage", object: sendData)
            })
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancelAction(sender: AnyObject) {
        switch currentView! {
            case .Image:
                transitionToInnerView()
        default:
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    // Text View
    
    func textViewDidChange(textView: UITextView) {
        let len = textView.text.characters.count
        charCounter.text = NSString(format: "%i", CHARACTERS_MAX - len) as String
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        return (textView.text.utf16.count + text.utf16.count - range.length) <= CHARACTERS_MAX
    }
    
    // Transition Animation
    
    func transitionToImageSelection() {
        self.navigationBar.alpha = 0.0
        UIView.animateWithDuration(0.2,
            animations: {
                let width = self.OT_SIZE.width - 50
                let height = self.OT_SIZE.height - 150
                self.innerView.alpha = 0.0
                self.cancelButton.alpha = 0.0
                self.confirmButton.alpha = 0.0
                self.preferredContentSize = CGSizeMake(width, height)
            },
            completion: { (finished) -> Void in
                self.createImageSelectionViewButtons()
            })
    }
    
    func reverseImageSelection() {
        UIView.animateWithDuration(0.3,
            animations: {
                self.innerView.alpha = 1.0
                self.cancelButton.alpha = 1.0
                self.confirmButton.alpha = 1.0
                self.navigationBar.alpha = 1.0
                self.preferredContentSize = self.OT_SIZE
            },
            completion: nil)
    }
    
    func transitionToImageView() {
        currentView = .Image
        UIView.animateWithDuration(0.2,
            animations: {
                self.preferredContentSize = CGSizeMake(self.OT_SIZE.width, self.TRANSITION_VIEW_HEIGHT)
            },
            completion: { (finished) -> Void in
                ConstraintManager.completeAddToSubview(self.mediaView, subView: self.imageView)
            })
    }
    
    func transitionToVideoView(videoURL: NSURL) {
        currentView = .Video
        UIView.animateWithDuration(0.2,
            animations: {
                self.innerView.alpha = 0.0
                self.preferredContentSize = CGSizeMake(self.OT_SIZE.width, self.TRANSITION_VIEW_HEIGHT)
            },
            completion: { (finished) -> Void in
                self.setMoviePlayerView(videoURL)
            })
    }
    
    func transitionToInnerView() {
        currentView = .Text
        clearMediaView()
        UIView.animateWithDuration(0.3,
            animations: {
                self.innerView.alpha = 1.0
                self.preferredContentSize = self.OT_SIZE
            }, completion: nil)
    }
    
    // Call Data
    
    func mediaFixture(notification: NSNotification) {
        let receivedData = notification.object as! [AnyObject]
        let imagePicker = receivedData[0] as! UIImagePickerController
        let info = receivedData[1] as! [String : AnyObject]
        
        if let _ = info[UIImagePickerControllerOriginalImage] as? UIImage {
            transitionToImageView()
            imageManager!.completeImagePicker(controller: imagePicker, dictionary: info, imageView: imageView)
        }
        else if let mediaType = info[UIImagePickerControllerMediaType] {
            if CFStringCompare(mediaType as! CFString, kUTTypeMovie, CFStringCompareFlags(rawValue: 0)) == CFComparisonResult.CompareEqualTo {
                let videoURL = info[UIImagePickerControllerMediaURL] as! NSURL
                let moviePath = NSString(string: videoURL.path!)
                if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(moviePath as String) {
                    transitionToVideoView(videoURL)
                }
            }
            if UIApplication.sharedApplication().isIgnoringInteractionEvents() {
                UIApplication.sharedApplication().endIgnoringInteractionEvents()
            }
        }
    }
    
    func setMoviePlayerView(videoURL: NSURL) {
        moviePlayer = MPMoviePlayerController(contentURL: videoURL)
        if let player = moviePlayer {
            player.view.frame = videoView.bounds
            player.prepareToPlay()
            player.scalingMode = .AspectFill
            videoView.addSubview(player.view)
        }
        
        self.view.addSubview(videoView)
    }
    
    // Image (Type) Selection View
    
    func createImageSelectionViewButtons() {
        let width: CGFloat = self.preferredContentSize.width / 1.7
        let height: CGFloat = self.preferredContentSize.height / 2
        let rightPosition = self.preferredContentSize.width * 0.38
        let leftPosition = self.preferredContentSize.width * 0.038
        let bottomPosition = self.preferredContentSize.height * 0.5
        let color = UIColor(red: 108.0/255.0, green: 151.0/255.0, blue: 202.0/255.0, alpha: 1.0)
        
        let returnButton = UIButton(frame: CGRectMake(rightPosition, 0, width, height))
        let captureButton = UIButton(frame: CGRectMake(leftPosition, bottomPosition, width, height))
        let retrieveButton = UIButton(frame: CGRectMake(rightPosition, bottomPosition, width, height))
        
        returnButton.setTitle("Return", forState: UIControlState.Normal)
        captureButton.setTitle("Take Photo", forState: UIControlState.Normal)
        retrieveButton.setTitle("Retrieve Photo", forState: UIControlState.Normal)
        
        returnButton.setTitleColor(color, forState: UIControlState.Normal)
        captureButton.setTitleColor(color, forState: UIControlState.Normal)
        retrieveButton.setTitleColor(color, forState: UIControlState.Normal)
        
        returnButton.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 17)
        captureButton.titleLabel?.font = UIFont(name: "Helvetica", size: 17)
        retrieveButton.titleLabel?.font = UIFont(name: "Helvetica", size: 17)
        
        returnButton.tintColor = color
        captureButton.tintColor = color
        retrieveButton.tintColor = color
        
        returnButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        captureButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        retrieveButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        
        returnButton.addTarget(self, action: #selector(OTView.returnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        captureButton.addTarget(self, action: #selector(OTView.captureAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        retrieveButton.addTarget(self, action: #selector(OTView.retrieveAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        returnButton.tag = 912
        captureButton.tag = 923
        retrieveButton.tag = 934
        
        view.addSubview(returnButton)
        view.addSubview(captureButton)
        view.addSubview(retrieveButton)
    }
    
    @objc func returnAction(sender: UIButton!) {
        view.viewWithTag(912)?.removeFromSuperview()
        view.viewWithTag(923)?.removeFromSuperview()
        view.viewWithTag(934)?.removeFromSuperview()
        reverseImageSelection()
    }
    
    @objc func captureAction(sender: UIButton!) {
        let call = "photoCall-post"
        self.dismissViewControllerAnimated(true, completion: nil)
        NSNotificationCenter.defaultCenter().postNotificationName("openThought-calls", object: call)
    }
    
    @objc func retrieveAction(sender: UIButton!) {
        let call = "photoCall-get"
        self.dismissViewControllerAnimated(true, completion: nil)
        NSNotificationCenter.defaultCenter().postNotificationName("openThought-calls", object: call)
    }
    
    // Clear Media View(s)
    
    func clearMediaView() {
        if let sub = mediaView.viewWithTag(DataTag.Image.rawValue) {
            sub.removeFromSuperview()
        }
        if let sub = mediaView.viewWithTag(DataTag.Video.rawValue) {
            sub.removeFromSuperview()
        }
    }
}
