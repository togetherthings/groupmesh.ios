//
//  ChatView.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 3/14/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit
import Darwin
import MultipeerConnectivity

class ChatView: JSQMessagesViewController {
    
    let incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImageWithColor(UIColor.jsq_messageBubbleBlueColor())
    let outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImageWithColor(UIColor.jsq_messageBubbleLightGrayColor())
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    let diameter: UInt = UInt(38 - CGFloat(M_PI))
    
    var messages = [JSQMessage]()
    var avatars = [String : JSQMessagesAvatarImage]()
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.hidden = true
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setup()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatView.handleMPCReceivedDataWithNotification(_:)), name: "receivedMPCDataNotification", object: nil)
        
        // Chat Setting
        inputToolbar!.contentView!.leftBarButtonItem = nil
        automaticallyScrollsToMostRecentMessage = true
        showLoadEarlierMessagesHeader = true
        self.collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width: 35.0, height: 35.0)
        self.collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: 35.0, height: 35.0)
    }
    
    override func viewWillDisappear(animated : Bool) {
        super.viewWillDisappear(animated)
        
        if (self.isMovingFromParentViewController()) {
            // Add exit chat message
            self.tabBarController?.tabBar.hidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup() {
        self.senderId = UIDevice.currentDevice().identifierForVendor?.UUIDString
        self.senderDisplayName = user.getFullName()
    }
    
    func setAvatar(image: UIImage, toUser: String) {
        let circleImage = JSQMessagesAvatarImageFactory.circularAvatarImage(image, withDiameter: diameter)
        let avatar = JSQMessagesAvatarImage(placeholder: circleImage)
        avatars[toUser] = avatar
    }
    
    func reloadMessagesView() {
        self.collectionView?.reloadData()
    }
    
    func handleMPCReceivedDataWithNotification(notification: NSNotification) {
        let receivedDataDictionary = notification.object as! Dictionary<String, AnyObject>
        let data = receivedDataDictionary["data"] as? NSData
        let fromPeer = receivedDataDictionary["fromPeer"] as! MCPeerID
        let dataDictionary = NSKeyedUnarchiver.unarchiveObjectWithData(data!) as! Dictionary<String, AnyObject>
        
        if let message = dataDictionary["message"] as? JSQMessage {
            if message != "_end_chat_" {
                setAvatar(fromPeer.displayImage!, toUser: fromPeer.displayName)
                messages.append(message)
                
                // Reload the tableview data and scroll to the bottom using the main thread.
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                    self.reloadMessagesView()
                })
            }
            else {
                print("Test")
                let alert = UIAlertController(title: "", message: "\(fromPeer.displayName) ended this chat.", preferredStyle: UIAlertControllerStyle.Alert)
                
                let doneAction: UIAlertAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default) { (alertAction) -> Void in
                    self.appDelegate.mpcManager.session.disconnect()
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
                
                alert.addAction(doneAction)
                
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                    self.presentViewController(alert, animated: true, completion: nil)
                })
            }
        }
    }
}

//MARK - Data Source
extension ChatView {
    
    func receivedMessagePressed(sender: UIBarButtonItem) {
        // Simulate reciving message
        showTypingIndicator = !showTypingIndicator
        scrollToBottomAnimated(true)
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    // Display Messages
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        let message = self.messages[indexPath.row]
        return message
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, didDeleteMessageAtIndexPath indexPath: NSIndexPath!) {
        self.messages.removeAtIndex(indexPath.row)
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        let data = messages[indexPath.row]
        switch(data.senderId) {
        case self.senderId:
            return self.outgoingBubble
        default:
            return self.incomingBubble
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.item]
        if message.senderDisplayName == self.senderDisplayName { cell.textView!.textColor = UIColor.blackColor() }
        else { cell.textView!.textColor = UIColor.whiteColor() }
        
        let attributes : [String:AnyObject] = [NSForegroundColorAttributeName:cell.textView!.textColor!, NSUnderlineStyleAttributeName: 1]
        cell.textView!.linkTextAttributes = attributes
        
        return cell
    }
    
    // Display Image
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
        let sender = messages[indexPath.row].senderDisplayName
        let avatar = avatars[sender]
        return avatar
    }
    
    // Display Name
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.item];
        
        // Skip name for self
        if message.senderDisplayName == self.senderDisplayName {
            return nil;
        }
        
        // Skip name for previous
        if indexPath.item > 0 {
            let previousMessage = messages[indexPath.item - 1];
            if previousMessage.senderDisplayName == message.senderDisplayName {
                return nil;
            }
        }
        
        return NSAttributedString(string: message.senderDisplayName)
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        let message = messages[indexPath.item]
        
        // Skip name for self
        if message.senderDisplayName == self.senderDisplayName {
            return CGFloat(0.0);
        }
        
        // Skip name for previous
        if indexPath.item > 0 {
            let previousMessage = messages[indexPath.item - 1];
            if previousMessage.senderDisplayName == message.senderDisplayName {
                return CGFloat(0.0);
            }
        }
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
}

//MARK - Toolbar
extension ChatView {
    
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
        let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text)
        let messageDictionary: [String : AnyObject] = ["message" : message]
        // This is the new version for more than 2 peers
        // We should probably add the peers for which the message fails to send to a list, and attempt
        // to resend the message to only those peers.
        var success = true
        for peer in appDelegate.mpcManager.session.connectedPeers {
            if !appDelegate.mpcManager.sendData(dictionaryWithData: messageDictionary, toPeer: peer) {
                success = false
            }
        }
        if success {
            setAvatar(user.adImage, toUser: senderDisplayName)
            messages.append(message)
            finishSendingMessage()
        } else {
            print("Could not send data to some peers")
        }
    }
    
    override func didPressAccessoryButton(sender: UIButton!) {
        
    }
}