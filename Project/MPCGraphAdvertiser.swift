//
//  MPCGraphAdvertiser.swift
//  ChatBox
//
//  Created by Jonathan Noyola on 5/19/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import MultipeerConnectivity

class MPCGraphAdvertiser: NSObject, MCNearbyServiceAdvertiserDelegate {

    var myPeer: MCPeerID!
    
    var mpcGraph: MPCGraph!
    
    var mpcSessionManager: MPCSessionManager!

    var mpcAdvertiser: MCNearbyServiceAdvertiser?
    
    init(myPeer: MCPeerID, mpcGraph: MPCGraph, mpcSessionManager: MPCSessionManager) {
        super.init()
        
        self.myPeer = myPeer
        self.mpcGraph = mpcGraph
        self.mpcSessionManager = mpcSessionManager
    }
    
    func startAdvertising() {
        mpcAdvertiser = MCNearbyServiceAdvertiser(peer: myPeer, discoveryInfo: getDiscoveryInfo(), serviceType: "chatbox-mpc")
        mpcAdvertiser!.delegate = self
        mpcAdvertiser!.startAdvertisingPeer()
    }
    
    func stopAdvertising() {
        mpcAdvertiser?.stopAdvertisingPeer()
        mpcAdvertiser?.delegate = nil
        mpcAdvertiser = nil
    }
    
    func getDiscoveryInfo() -> [String: String] {
        var info = [String: String]()
        info["graphID"] = mpcGraph.graphID
        return info
    }
    
    /////////////////////////////////////////////
    // MARK: MCNearbyServiceAdvertiserDelegate //
    /////////////////////////////////////////////
    
    func advertiser(advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: NSData?, invitationHandler: (Bool, MCSession) -> Void) {
        let session = mpcSessionManager.chooseAndIncrementSession()
        invitationHandler(true, session)
    }
    
    func advertiser(advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: NSError) {
        print(error.localizedDescription)
    }
}
