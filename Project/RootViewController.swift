//
//  RootViewController.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 2/4/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import CoreData

class RootViewController: UITabBarController {

    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load User Information
        User.loadUser()
        print("User Loaded")
        
        // Listen for invitations/connections on any page
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.invitationWasReceived(_:)), name: "invitationWasReceived", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.connectedWithPeer(_:)), name: "connectedWithPeer", object: nil)
        
        // Set bluetooth function
        appDelegate.mpcManager = MPCManager()
        appDelegate.mpcManager.advertiser.startAdvertisingPeer()
        appDelegate.mpcManager.delegate = MPCBrowser()
        appDelegate.mpcManager.browser.startBrowsingForPeers()
        
        // load views
        loadViews()
    }
    
    
    func loadViews() {
        let viewControllers = self.viewControllers as! [UINavigationController]
        for nav in viewControllers {
            let navView = nav.viewControllers[0]
            print(navView.title)
            let _ = navView.view
        }
    }
    
    func invitationWasReceived(notification: NSNotification) {
        let fromPeer = notification.object as! MCPeerID
    
        let alert = UIAlertController(title: "", message: "\(fromPeer.displayName) wants to chat with you.", preferredStyle: UIAlertControllerStyle.Alert)
            
        let acceptAction: UIAlertAction = UIAlertAction(title: "Accept", style: UIAlertActionStyle.Default) { (alertAction) -> Void in
            let data = ["chat"]
            let methodData = NSKeyedArchiver.archivedDataWithRootObject(data)
            self.appDelegate.mpcManager.connectedWithPeer(self.appDelegate.mpcManager.peer)
            self.appDelegate.mpcManager.browser.invitePeer(fromPeer, toSession: self.appDelegate.mpcManager.session, withContext: methodData, timeout: 20)
            //self.appDelegate.mpcManager.invitationHandler(true, self.appDelegate.mpcManager.session)
        }
        
        let declineAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (alertAction) -> Void in
            //self.appDelegate.mpcManager.invitationHandler(false, self.appDelegate.mpcManager.session)
        }
        
        alert.addAction(acceptAction)
        alert.addAction(declineAction)
        
        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func connectedWithPeer(notification: NSNotification) {
        //let peerID = notification.object as! MCPeerID
        
        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            self.performSegueWithIdentifier("chatview", sender: self)
            self.navigationItem.title = "Exit"
        }
    }
}
