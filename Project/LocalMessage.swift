//
//  Message.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 5/16/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

class LocalMessage: NSObject {
    
    var title: NSString?
    var author: NSString!
    var displayImage: UIImage!
    var message: NSString?
    var media: NSArray?
    var mediaType: DataType!
    
    // Time
    var date: NSDate!
    var dateFormatter: NSDateFormatter!

    init(title: String?, author: String, displayImage: UIImage, message: String?, media: NSArray?, mediaType: DataType) {
        super.init()
        self.title = NSString(string: title!)
        self.author = NSString(string: author)
        self.displayImage = displayImage
        self.message = NSString(string: message!)
        self.media = media
        self.mediaType = mediaType
        setTimeStamp()
    }
    
    init(notification: NSNotification) {
        super.init()
        if let received = notification.object as? [String : AnyObject] {
            let author = received["author"] as! String
            let displayImage = received["displayImage"] as! UIImage
            self.author = NSString(string: author)
            self.displayImage = displayImage
            if let title = received["title"] as? String {
                self.title = NSString(string: title)
            }
            if let message = received["message"] as? String {
                self.message = NSString(string: message)
                self.mediaType = DataType.Text
            }
            if let media = received["media"] as? [AnyObject] {
                let type = received["mediaType"] as! String
                self.media = NSArray(array: media)
                self.mediaType = DataType(rawValue: type)
            }
        }
        else {
            let error = "Notification Error"
            self.author = error
            self.message = error
            self.displayImage = UIImage()
            self.mediaType = DataType.Text
        }
        setTimeStamp()
    }
    
    func setTimeStamp() {
        date = NSDate()
        dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, hh:mm a"
    }
    
    func getTitle() -> String {
        var string = ""
        if let _ = title {
            string = String(title!)
        }
        return string
    }
    
    func getAuthor() -> String {
        return String(author)
    }
    
    func getMessage() -> String {
        var string = ""
        if let _ = message {
            string = String(message!)
        }
        return string
    }
    
    func getTimeStamp() -> String {
        return dateFormatter.stringFromDate(date)
    }
    
    func getMedia() -> AnyObject {
        if hasMedia() {
            return (media?.objectEnumerator())!
        }
        return []
    }
    
    func hasMedia() -> Bool {
        if let _ = media {
            return true
        }
        
        return false
    }
}
