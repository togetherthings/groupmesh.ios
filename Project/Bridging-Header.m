//
//  Bridging-Header.m
//  ChatBox
//
//  Created by Leo Da Costa on 2/24/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bridging-Header.h"

@interface ObjectiveCFile ()
@end
@implementation ObjectiveCFile
- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void) displayMessageFromCreatedObjectiveCFile {
    NSLog(@"Hello. . . ");
    NSLog(@"This Message From Created Objective-C File with Swift Project");
}
@end