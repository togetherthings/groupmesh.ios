//
//  ViewController.swift
//  GroupMesh
//  Created by Leonel Da Costa on 7/22/16.
//  Copyright (c) TogetherThings. All rights reserved.

import UIKit
import MultipeerConnectivity
import Darwin

class MPCBrowser: NSObject, MPCManagerDelegate {
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var peers: [MCPeerID]!
    var currentMesh: [MeshGroup]!
    
    override init() {
        super.init()
        
        peers = appDelegate.mpcManager.foundPeers

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MPCBrowser.advertiserImageUpdateNotification(_:)), name: "receivedImageUpdateNotification", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MPCBrowser.requestFoundPeers(_:)), name: "requestFoundPeers", object: nil)
    }
    
    func requestFoundPeers(notification: NSNotification) {
        foundPeer()
    }
    
    // MARK: MPCManagerDelegate
    
    func foundPeer() {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            var data = [String : [MCPeerID]]()
            data["general"] = Array(Set(self.peers))
            NSNotificationCenter.defaultCenter().postNotificationName("Peers", object: data)
        })
    }
    
    
    func lostPeer() {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            var data = [String : [MCPeerID]]()
            data["general"] = Array(Set(self.peers))
            NSNotificationCenter.defaultCenter().postNotificationName("Peers", object: data)
        })
    }
    
    func sendDisplayImage() {
        appDelegate.mpcManager.sendPublicImage(user.adImage)
    }
    
    func automaticInvitation() {
        self.appDelegate.mpcManager.invitationHandler(true, self.appDelegate.mpcManager.session)
    }
    
    func advertiserImageUpdateNotification(notification: NSNotification) {
        let receivedData = notification.object as! Dictionary<String, AnyObject>
        let fromPeer = receivedData["fromPeer"] as? MCPeerID
        let image = receivedData["image"] as? UIImage
        
        peers = appDelegate.mpcManager.foundPeers
        if let peerInSession = peers.indexOf(fromPeer!) {
            peers[peerInSession].displayImage = image
            print("setting image from \(peers[peerInSession].displayName)")
            print(peers[peerInSession].displayImage)
        }
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            var data = [String : [MCPeerID]]()
            data["general"] = self.peers
            NSNotificationCenter.defaultCenter().postNotificationName("Peers", object: data)
        })
    }
    
    
    // MESH!

    func setCurrentMesh(name string: String) -> Void {
        if currentMesh == nil {
            currentMesh = [MeshGroup]()
            currentMesh.append(MeshGroup(name: string))
        }
        else {
            currentMesh.append(MeshGroup(name: string))
        }
    }
    
    func sendCurrentMesh(groupMesh group: MeshGroup) {
        let mesh = ["sendCurrentMesh", group.name, group.members]
        let methodData = NSKeyedArchiver.archivedDataWithRootObject(mesh)
        for peer in group.members {
            appDelegate.mpcManager.browser.invitePeer(peer as! MCPeerID, toSession: appDelegate.mpcManager.session, withContext: methodData, timeout: 20)
        }
    }
    
    func meshInvitation(fromPeer: String, group: MeshGroup) {
        //self.appDelegate.mpcManager.invitationHandler(true, self.appDelegate.mpcManager.session)
        print(group.members)
        self.setCurrentMesh(name: group.name)
        self.currentMesh[self.currentMesh.count - 1].addSession(self.appDelegate.mpcManager.session)
    }
}

