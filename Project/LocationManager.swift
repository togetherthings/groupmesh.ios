//
//  LocationManager.swift
//  GroupMesh
//
//  Created by Leo Da Costa on 7/23/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import Foundation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    var manager = CLLocationManager()
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override init() {
        super.init()
        manager.delegate = self
    }
    
    func start() {
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    func getCoordinates() -> Dictionary<String, Double> {
        let latitude = Double((manager.location?.coordinate.latitude)!)
        let longitude = Double((manager.location?.coordinate.longitude)!)
        
        return ["latitude" : latitude, "longitude" : longitude]
    }
    
    func getCoordinatesAsString() -> String {
        let latitude = Double((manager.location?.coordinate.latitude)!)
        let longitude = Double((manager.location?.coordinate.longitude)!)
        let coordinates = "\(latitude), \(longitude)"
        
        return coordinates
    }
    
    
    // MARK: - Location Manager Functions
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation: CLLocation = locations[0]
        CLGeocoder().reverseGeocodeLocation(userLocation, completionHandler: { (placemarks, error) -> Void in
            if error != nil {
                print("\(error)")
            }
            else if placemarks?.count > 0 {
                let p = CLPlacemark(placemark: placemarks![0] as CLPlacemark)
                //self.organizationLabel.text = p.areasOfInterest![0]
                if p.areasOfInterest != nil {
                    //self.organizationLabel.text = p.areasOfInterest![0]
                }
                else if p.subLocality != nil {
                    //self.organizationLabel.text = p.subLocality
                }
                else {
                    //self.organizationLabel.text = p.locality
                }
            }
        })
    }
}
