//
//  ProfileEditInfoCell.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 3/8/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

class ProfileEditInfoCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var view: UIView!
    
    var data = [String]()
    var modified = false
    var connectedCell: ProfileEditInfoCell!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Set Button
        addButton.layer.cornerRadius = 3
        
        // Set View
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    
    @IBAction func addAction(sender: AnyObject) {
        let info = textField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if info.characters.count > 0 {
            data.append(info)
            updateTableView()
            textField.text = ""
            modified = true
        }
        else {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let viewController = appDelegate.window?.rootViewController as? EditProfileViewController
            let alertController = UIAlertController(title: "Error", message: "Empty field", preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(okAction)
            dispatch_async(dispatch_get_main_queue(), { ()
                viewController!.presentViewController(alertController, animated: true, completion: nil)
            })
        }
    }
    
    // Resign First Responder
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        self.tableView.endEditing(true)
    }
    
    // MARK: - Table View
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = data[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            handleCellDeletion(tableView, indexPath: indexPath)
        }
    }
    
    func updateTableView() {
        tableView.reloadData()
        if tableView.contentSize.height > tableView.frame.size.height {
            let count = data.count
            tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: count - 1, inSection: 0), atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
        }
    }
    
    func handleCellDeletion(tableView: UITableView, indexPath: NSIndexPath) {
        var combined: Int
        if let connected = connectedCell {
            combined = data.count + connected.data.count
        }
        else { combined = user.organizations.count + user.interests.count }
        if combined > 1 {
            tableView.beginUpdates()
            data.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            tableView.endUpdates()
            modified = true
        }
        else {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let viewController = appDelegate.window?.rootViewController as? EditProfileViewController
            let alertController = UIAlertController(title: "Error", message: "Must have at least one organization or interest", preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(okAction)
            dispatch_async(dispatch_get_main_queue(), { ()
                viewController!.presentViewController(alertController, animated: true, completion: nil)
            })
        }
    }
}
