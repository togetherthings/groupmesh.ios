//
//  RegistrationCompleteCell.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 3/22/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit

class RegistrationCompleteCell: UITableViewCell {
    
    @IBOutlet weak var completeButton: UIButton!
    
    var dataTable: CreateUserDataTable!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        completeButton.layer.cornerRadius = 3
        completeButton.layer.borderWidth = 0.5
        completeButton.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    @IBAction func completeButtonAction(sender: AnyObject) {
        if dataTable.hasMainInfo() && dataTable.hasSubInfo() {
            let organizations = dataTable.cells[0] as! RegistrationInfoCell
            let interests = dataTable.cells[1] as! RegistrationInfoCell
            
            var data = dataTable.data
            data["interests"] = interests.data
            data["organizations"] = organizations.data
            data["usertype"] = "new"
            user.save(data)
            
            dataTable.complete()
        }
    }
}
