//
//  RegisterInfoCell.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 3/22/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit

class RegistrationInfoCell: UITableViewCell, UICollectionViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var informationTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var informationView: UIView!
    
    var data = [String]()
    var collectionViewDataSource: CollectionViewDataSource!
    var cellSizeCache = NSCache()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Configure collection view
        collectionViewDataSource = CollectionViewDataSource(collectionView: collectionView)
        collectionView.registerClass(CollectionViewCell.self, forCellWithReuseIdentifier: CollectionViewCell.Const.ReuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = collectionViewDataSource
        
        let color = UIColor(red: 108.0/255.0, green: 151.0/255.0, blue: 202.0/255.0, alpha: 1.0)
        collectionView.layer.borderColor = color.CGColor
        collectionView.layer.borderWidth = 0.5
        collectionView.layer.cornerRadius = 3
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5)
        layout.minimumInteritemSpacing = 2
        
        // Set Text Field
        informationTextField.delegate = self
        
        // Set View
        informationView.layer.borderWidth = 0.5
        informationView.layer.borderColor = UIColor.lightGrayColor().CGColor
    }

    @IBAction func addButtonAction(sender: AnyObject) {
        let information = informationTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if informationTextField.text?.characters.count > 0 {
            data.append(information)
            informationTextField.text = ""
            
            collectionViewDataSource.addData(data)
        }
        else {
            print("No text")
        }
    }
    
    // Resign First Responder
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if informationTextField.isFirstResponder() {
            informationTextField.resignFirstResponder()
        }
        
        return true
    }
}

// MARK: - UICollectionViewFlowLayout Delegate

extension RegistrationInfoCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        // If fitted size was computed in the past for this cell, return it from cache
        if let size = cellSizeCache.objectForKey(indexPath) as? NSValue {
            return size.CGSizeValue()
        }
        
        let cell = collectionViewDataSource.configuredCellForIndexPath(indexPath, prototype: true) as! ContentAwareCollectionViewCell
        // Set a constrained size for the cell
        let width: CGFloat = collectionView.bounds.width / 3 - 10
        let height = CGFloat.max
        let constrainedSize = CGSize(width: width, height: height)
        let size = cell.fittedSizeForConstrainedSize(constrainedSize)
        
        cellSizeCache.setObject(NSValue(CGSize: size), forKey: indexPath)
        
        return size
    }
}
