//
//  EditProfileViewController.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 1/10/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit
import CoreData

class EditProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    enum TextFields: Int {
        case FirstName
        case LastName
        case Email
        case Information
    }
    
    enum Labels: Int {
        case Header
        case FirstName
        case LastName
        case Email
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    weak var activeTextField: UITextField?
    var isViewRaised = false
    var kbHeight: CGFloat = 0
    
    var cells = [UITableViewCell]()
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //obtainUserData()
        self.tabBarController?.tabBar.hidden = true
        self.navigationController!.navigationBar.barStyle = .Default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // TableView
        let color = UIColor(red: 108.0/255.0, green: 151.0/255.0, blue: 202.0/255.0, alpha: 1.0).CGColor
        tableView.layer.cornerRadius = 3
        tableView.layer.borderWidth = 0.5
        tableView.layer.borderColor = color
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(EditProfileViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(EditProfileViewController.keyboardWillBeHidden(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Save
    
    @IBAction func saveAction(sender: AnyObject) {
        // Cells
        var mainCell: ProfileEditMainCell!
        var firstInfoCell = ProfileEditInfoCell()
        var secondInfoCell = ProfileEditInfoCell()
        for i in 0 ..< cells.count {
            let cell = cells[i]
            if i == 0 { mainCell = cell as! ProfileEditMainCell }
            else if i == 1 { firstInfoCell = cell as! ProfileEditInfoCell }
            else if i == 2 { secondInfoCell = cell as! ProfileEditInfoCell }
        }
        
        let firstNameText = mainCell.firstNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let lastNameText = mainCell.lastNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let emailText = mainCell.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        var changed = [String : AnyObject]()
        if firstInfoCell.modified == true { changed["organizations"] = firstInfoCell.data }
        if secondInfoCell.modified == true { changed["interests"] = secondInfoCell.data }
        if firstNameText.characters.count > 0 && lastNameText.characters.count > 0 {
            if firstNameText.characters.count < 2 || lastNameText.characters.count < 2 {
                // Add alert
                let alertController = UIAlertController(title: "Error", message: "Name is too short", preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(okAction)
                dispatch_async(dispatch_get_main_queue(), { ()
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            }
            else {
                let name = [firstNameText, lastNameText]
                changed["name"] = name
            }
        }
        else if firstNameText.characters.count > 0 {
            if firstNameText.characters.count < 2 {
                // Add alert
                let alertController = UIAlertController(title: "Error", message: "Name is too short", preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(okAction)
                dispatch_async(dispatch_get_main_queue(), { ()
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            }
            else {
                let name = [firstNameText, user.lastName]
                changed["name"] = name
            }
        }
        else if lastNameText.characters.count > 0 {
            if lastNameText.characters.count < 2 {
                // Add alert
                let alertController = UIAlertController(title: "Error", message: "Name is too short", preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(okAction)
                dispatch_async(dispatch_get_main_queue(), { ()
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            }
            else {
                let name = [user.firstName, lastNameText]
                changed["name"] = name
            }
        }
        if emailText.characters.count > 0 {
            if emailText.characters.count < 6 {
                // Add alert
                let alertController = UIAlertController(title: "Error", message: "Please enter a proper email account", preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(okAction)
                dispatch_async(dispatch_get_main_queue(), { ()
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            }
            else {
                let email = emailText
                changed["email"] = email
            }
        }
        
        // Apply Changes
        if changed.count > 0 {
            if let name = changed["name"] as? [String] {
                if user.hasDefaultImage == true { changed["image"] = [DefaultImage.makeUserDefaultImage(name), user.hasDefaultImage] }
            }

            user.save(changed)
            updateProfile(changed)
            
            let alertController = UIAlertController(title: "Saved", message: "User information has been updated", preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(okAction)
            dispatch_async(dispatch_get_main_queue(), { ()
                self.presentViewController(alertController, animated: true, completion: nil)
            })
        }
    }
    
    
    // MARK: - TableView
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 1 {
            return 224.0
        }
        else if indexPath.row == 3 || indexPath.row == 5 {
            return 485.0
        }
        else {
            return 37.0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("mainCell")! as! ProfileEditMainCell
            let data = ["first" : user.firstName, "last" : user.lastName, "email" : user.email]
            cell.addPlaceHolders(data)
            // Delegates
            cell.firstNameTextField.delegate = self
            cell.lastNameTextField.delegate = self
            cell.emailTextField.delegate = self
            // Tags
            cell.firstNameTextField.tag = TextFields.FirstName.rawValue
            cell.lastNameTextField.tag = TextFields.LastName.rawValue
            cell.emailTextField.tag = TextFields.Email.rawValue
            cell.firstNameLabel.tag = Labels.FirstName.rawValue
            cell.lastNameLabel.tag = Labels.LastName.rawValue
            cell.emailLabel.tag = Labels.Email.rawValue
            if cells.count < 3 { cells.append(cell) }
            return cell
        }
        else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCellWithIdentifier("infoCell")! as! ProfileEditInfoCell
            cell.tableView.delegate = cell
            cell.tableView.dataSource = cell
            cell.textField.placeholder = "Add a job, school, group, etc"
            cell.textField.delegate = self
            cell.textField.tag = TextFields.Information.rawValue
            cell.addButton.tag = 100
            cell.tableView.tag = 200
            if let organizations = Optional(user.organizations) { cell.data = organizations }
            if cells.count < 3 { cells.append(cell) }
            return cell
        }
        else if indexPath.row == 5 {
            let cell = tableView.dequeueReusableCellWithIdentifier("infoCell")! as! ProfileEditInfoCell
            cell.tableView.delegate = cell
            cell.tableView.dataSource = cell
            cell.textField.placeholder = "Add a study, sport, hobby, etc"
            cell.textField.delegate = self
            cell.textField.tag = TextFields.Information.rawValue
            cell.addButton.tag = 100
            cell.tableView.tag = 200
            if let interests = Optional(user.interests) { cell.data = interests }
            let connectCell = cells[1] as! ProfileEditInfoCell
            connectCell.connectedCell = cell
            cell.connectedCell = connectCell
            if cells.count < 3 { cells.append(cell) }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("header")! as! ProfileEditHeaderCell
            if indexPath.row == 0 { cell.textLabel?.text = "User" }
            if indexPath.row == 2 { cell.textLabel?.text = "Organization" }
            else if indexPath.row == 4 { cell.textLabel?.text = "Interest" }
            cell.textLabel?.tag = Labels.Header.rawValue
            cell.textLabel?.textAlignment = .Center
            return cell
        }
    }
    
    // Update Profile
    
    func updateProfile(data: [String: AnyObject]) {
        let controller = tabBarController as! RootViewController
        let navigation = controller.viewControllers![2] as! UINavigationController
        let profile = navigation.viewControllers[0] as! ProfileView
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            profile.setProfile(data)
        })
    }
    
    
    // Resign First Responder
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Cells
        let mainCell = cells[0] as! ProfileEditMainCell
        let firstInfoCell = cells[1] as! ProfileEditInfoCell
        let secondInfoCell = cells[2] as! ProfileEditInfoCell
        
        let firstNameTextField = mainCell.firstNameTextField
        let lastNameTextField = mainCell.lastNameTextField
        let emailTextField = mainCell.emailTextField
        let organizationTextField = firstInfoCell.textField
        let interestTextField = secondInfoCell.textField
        
        // Responders
        if firstNameTextField.isFirstResponder() {
            firstNameTextField.resignFirstResponder()
            lastNameTextField.becomeFirstResponder()
        }
        else if lastNameTextField.isFirstResponder() {
            lastNameTextField.resignFirstResponder()
            emailTextField.becomeFirstResponder()
        }
        else if emailTextField.isFirstResponder() {
            emailTextField.resignFirstResponder()
            organizationTextField.becomeFirstResponder()
        }
        else if organizationTextField.isFirstResponder() {
            organizationTextField.resignFirstResponder()
        }
        else if interestTextField.isFirstResponder() {
            interestTextField.resignFirstResponder()
        }
        return true
    }
    
    // Keyboard
    
    func textFieldDidBeginEditing(textField: UITextField) {
        activeTextField = textField
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        activeTextField = nil
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if activeTextField != nil && activeTextField!.tag == TextFields.Information.rawValue {
            isViewRaised = true
            animateView(notification, direction: -1)
        }
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        if isViewRaised {
            isViewRaised = false
            animateView(notification, direction: 1)
        }
    }
    
    func animateView(notification: NSNotification, direction: CGFloat) {
        let info: NSDictionary = notification.userInfo!
        let kbSize = info.objectForKey(UIKeyboardFrameEndUserInfoKey)?.CGRectValue.size
        kbHeight = max(kbHeight, kbSize!.height)
        
        UIView.beginAnimations("MoveViewForKeyboard", context: nil)
        UIView.setAnimationDuration(info.objectForKey(UIKeyboardAnimationDurationUserInfoKey) as! Double)
        UIView.setAnimationCurve(UIViewAnimationCurve(rawValue: info.objectForKey(UIKeyboardAnimationCurveUserInfoKey) as! Int)!)
        view.center = CGPoint(x: view.center.x, y: view.center.y + kbHeight * direction)
        UIView.commitAnimations()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}