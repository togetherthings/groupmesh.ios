//
//  DashboardViewController.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 1/2/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

class DashboardViewController: UIViewController, CLLocationManagerDelegate, UITableViewDelegate {
    
    var locationManager = CLLocationManager()
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewWillAppear(animated: Bool) {
        // Geolocation
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Location Manager function
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation: CLLocation = locations[0]
        CLGeocoder().reverseGeocodeLocation(userLocation, completionHandler: { (placemarks, error) -> Void in
            if error != nil {
                //print(error)
            }
            else if placemarks?.count > 0 {
                let p = CLPlacemark(placemark: placemarks![0] as CLPlacemark)
                //self.organizationLabel.text = p.areasOfInterest![0]
                if p.areasOfInterest != nil {
                    //self.organizationLabel.text = p.areasOfInterest![0]
                }
                else if p.subLocality != nil {
                    //self.organizationLabel.text = p.subLocality
                }
                else {
                    //self.organizationLabel.text = p.locality
                }
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
