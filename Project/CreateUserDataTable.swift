//
//  CreateUserDataTable.swift
//  GroupMesh
//
//  Created by Leonel Da Costa on 3/22/16.
//  Copyright © 2016 TogetherThings. All rights reserved.
//

import UIKit

class CreateUserDataTable: UITableViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailAddressTextField: UITextField!
    
    @IBOutlet weak var uploadPictureButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    
    let imagePicker = UIImagePickerController()
    let tableCell = ["RegisterInfoCell", "RegisterInfoCell", "RegistrationComplete"]
    var cells = [UITableViewCell]()
    var data = [String : AnyObject]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        // Set Text Fields
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        emailAddressTextField.delegate = self
        
        // Set View
        mainView.layer.borderWidth = 0.5
        mainView.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        // Set Image View
        imagePicker.delegate = self
        imageView.layer.cornerRadius =  (imageView.frame.height / 2)
        imageView.contentMode = .ScaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        let color = UIColor(red: 108.0/255.0, green: 151.0/255.0, blue: 202.0/255.0, alpha: 1.0)
        let defaultText = "Default"
        let whiteImage = ImageManager.getImageWithColor(UIColor.whiteColor(), size: CGSizeMake(120, 120))
        let image = ImageManager.setTextOnImage(defaultText, image: whiteImage, color: color, withTextSize: 17)
        imageView.image = image
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(tableCell[indexPath.row], forIndexPath: indexPath)
        
        if indexPath.row == 0 {
            let cell = cell as! RegistrationInfoCell
            cell.titleLabel.text = "Organizations"
            cell.informationTextField.placeholder = "Job, school, clubs, etc"
            cells.append(cell)
            return cell
        }
        else if indexPath.row == 1 {
            let cell = cell as! RegistrationInfoCell
            cell.titleLabel.text = "Interests"
            cell.informationTextField.placeholder = "Exercising, science, movies, games, etc"
            cells.append(cell)
            return cell
        }
        
        // Set Default / Complete Cell
        let defaultCell = cell as! RegistrationCompleteCell
        defaultCell.dataTable = self
        
        return defaultCell
    }
    
    // Upload Image
    
    @IBAction func uploadPictureButtonAction(sender: AnyObject) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    // Mark: - UIImagePickerControllerDelegate methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let resizedImage = ImageManager.resizeImage(pickedImage, toTheSize: CGSizeMake(120, 120))
            imageView.image = resizedImage
            data["image"] = [resizedImage, false]
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: - Create User
    
    func hasMainInfo() -> Bool {
        let firstName = firstNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let lastName = lastNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let email = emailAddressTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        // Check for clearance
        if firstName.characters.count == 0 && lastName.characters.count == 0 && email.characters.count == 0 {
            // Add alert
            let alertController = UIAlertController(title: "Incomplete Setup", message: "Please enter both first and last name", preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(okAction)
            dispatch_async(dispatch_get_main_queue(), { ()
                self.presentViewController(alertController, animated: true, completion: nil)
            })
        }
        else if firstName.characters.count < 2 {
            // Add alert
            if firstName.characters.count == 0 {
                let alertController = UIAlertController(title: "Incomplete Setup", message: "Please enter a first name", preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(okAction)
                dispatch_async(dispatch_get_main_queue(), { ()
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            }
            else {
                let alertController = UIAlertController(title: "Error", message: "Name is too short", preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(okAction)
                dispatch_async(dispatch_get_main_queue(), { ()
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            }
        }
        else if lastName.characters.count < 2 {
            // Add alert
            if lastName.characters.count == 0 {
                let alertController = UIAlertController(title: "Incomplete Setup", message: "Please enter a last name", preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(okAction)
                dispatch_async(dispatch_get_main_queue(), { ()
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            }
            else {
                let alertController = UIAlertController(title: "Error", message: "Name is too short", preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(okAction)
                dispatch_async(dispatch_get_main_queue(), { ()
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            }
        }
        else if email.characters.count < 6 { // Rework on this process to accept actual emails
            // Add alert
            if email.characters.count == 0 {
                let alertController = UIAlertController(title: "Incomplete Setup", message: "Please enter an email address", preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(okAction)
                dispatch_async(dispatch_get_main_queue(), { ()
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            }
            else {
                let alertController = UIAlertController(title: "Error", message: "Please enter a proper email address", preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(okAction)
                dispatch_async(dispatch_get_main_queue(), { ()
                    self.presentViewController(alertController, animated: true, completion: nil)
                })
            }
        }
        else {
            let name = [firstName, lastName]
            data["name"] = name
            data["email"] = email
            return true
        }
        
        return false
    }
    
    func hasSubInfo() -> Bool {
        let combined = (cells[0] as! RegistrationInfoCell).data.count + (cells[1] as! RegistrationInfoCell).data.count
        if combined == 0 {
            let alertController = UIAlertController(title: "Incomplete Setup", message: "Must add at least one organization or interest", preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(okAction)
            dispatch_async(dispatch_get_main_queue(), { ()
                self.presentViewController(alertController, animated: true, completion: nil)
            })
            return false
        }
        
        return true
    }
    
    func complete() {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let viewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Root") as UIViewController
            self.presentViewController(viewController, animated: false, completion: nil)
        })
    }
    
    // Resign First Responder
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        backgroundView.endEditing(true)
        mainView.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if firstNameTextField.isFirstResponder() {
            firstNameTextField.resignFirstResponder()
            lastNameTextField.becomeFirstResponder()
        }
        else if lastNameTextField.isFirstResponder() {
            lastNameTextField.resignFirstResponder()
            emailAddressTextField.becomeFirstResponder()
        }
        else if emailAddressTextField.isFirstResponder() {
            emailAddressTextField.resignFirstResponder()
        }
        
        return true
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
