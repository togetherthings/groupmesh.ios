//
//  PageContainer.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 3/24/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

class ActivityPageContainer: UIViewController, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var textView: UITextView!
    var alert: UIAlertController!
    
    lazy var openThoughtButton: UIBarButtonItem = { return UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: #selector(ActivityPageContainer.openThought)) }()
    
    private(set) lazy var orderedViewControllers: [UIViewController] = { return [PageViewController.newActivityViewController("Activity"), PageViewController.newActivityViewController("Map")] }()
    var pageViewController: PageViewController? {
        didSet {
            pageViewController?.pageDelegate = self
            pageViewController?.orderedViewControllers = self.orderedViewControllers
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.addTarget(self, action: #selector(ActivityPageContainer.didChangePageControlValue), forControlEvents: .ValueChanged)
        openThoughtButton.tintColor = UIColor(red: 108.0/255.0, green: 151.0/255.0, blue: 202.0/255.0, alpha: 1.0)
        navigationBar.rightBarButtonItem = openThoughtButton
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ActivityPageContainer.serverMessage), name: "serverMessage", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let pageViewController = segue.destinationViewController as? PageViewController {
            self.pageViewController = pageViewController
        }
    }
    
    func didChangePageControlValue() {
        pageViewController?.scrollToViewController(index: segmentedControl.selectedSegmentIndex)
        // Add/Remove Post
        if segmentedControl.selectedSegmentIndex == 0 {
            navigationBar.rightBarButtonItem = openThoughtButton
        }
        else if segmentedControl.selectedSegmentIndex == 1 {
            navigationBar.rightBarButtonItem = nil
        }
    }
    
    func openThought() {
        NSNotificationCenter.defaultCenter().postNotificationName("openThought", object: nil)
    }
    
    
    @IBAction func connectAction(sender: AnyObject) {
        HTTPSServerAccess.login()
    }
    
    func serverMessage(notification: NSNotification) {
        var title = "Error"
        var message = "Request denied"
        if let text = notification.object {
            message = ""
            title = "Success"
            let response = text as! NSHTTPURLResponse
            let values = response.allHeaderFields.values
            for value in values {
                message += "\(value)\n"
            }
        }
        let alert = UIAlertController(title: "\(title)", message: "\(message)", preferredStyle: UIAlertControllerStyle.Alert)
        let okayAction: UIAlertAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default) { (alertAction) -> Void in
        }
        alert.addAction(okayAction)
        
        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
            self.presentViewController(alert, animated: true, completion: nil)
        })
    }
}

extension ActivityPageContainer: PageViewControllerDelegate {
    
    func pageViewController(tutorialPageViewController: PageViewController, didUpdatePageCount count: Int) {
        segmentedControl.selectedSegmentIndex = count
    }
    
    func pageViewController(tutorialPageViewController: PageViewController, didUpdatePageIndex index: Int) {
        segmentedControl.selectedSegmentIndex = index
        didChangePageControlValue()
    }
    
}