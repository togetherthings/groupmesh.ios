//
//  ProfileCell.swift
//  ChatBox
//
//  Created by Leonel Da Costa on 3/11/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import UIKit

class ProfileTableCell: UITableViewCell {

    var collectionViewDataSource: CollectionViewDataSource!
    var cellSizeCache = NSCache()
    var data: [String]!
    var height: CGFloat = 0.0
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Configure collection view
        collectionViewDataSource = CollectionViewDataSource(collectionView: collectionView, cell: self)
        collectionView.registerClass(CollectionViewCell.self, forCellWithReuseIdentifier: CollectionViewCell.Const.ReuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = collectionViewDataSource
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(1, 5, 0, 5)
        layout.minimumInteritemSpacing = 5
    }
    
    func instantiateData(data: [String]) {
        collectionViewDataSource.instantiateProfileData(data)
    }
    
    func makeTitle(title: String) {
        collectionViewDataSource.title = title
    }
    
    func makeData(data: [String]) {
        self.data = data
    }
    
    func getTitle() -> String {
        return collectionViewDataSource.title
    }
}

// MARK: - UICollectionViewFlowLayout Delegate

extension ProfileTableCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        // If fitted size was computed in the past for this cell, return it from cache
        if let size = cellSizeCache.objectForKey(indexPath) as? NSValue {
            return size.CGSizeValue()
        }
        
        let cell = collectionViewDataSource.configuredCellForIndexPath(indexPath, prototype: true) as! ContentAwareCollectionViewCell
        // Set a constrained size for the cell
        let width: CGFloat = collectionView.bounds.width / 3 - 10
        let height = CGFloat.max
        let constrainedSize = CGSize(width: width, height: height)
        let size = cell.fittedSizeForConstrainedSize(constrainedSize)
        
        cellSizeCache.setObject(NSValue(CGSize: size), forKey: indexPath)
        
        return size
    }
}
