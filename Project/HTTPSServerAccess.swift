//
//  HTTPServerAccess.swift
//  ChatBox
//
//  Created by Leo Da Costa on 6/5/16.
//  Copyright © 2016 ChatBox Team. All rights reserved.
//

import Foundation
import UIKit

enum HTTPMethod: String {
    case POST = "POST"
    case GET = "GET"
}

enum HTTPSessionType {
    case Default
    case Ephemeral
    case Background
}

enum HTTPSServerAccessPath: String {
    case Connect = "https://www.coremeridian.xyz/"
    case Register = "https://www.coremeridian.xyz/mobile/register"
    case Login = "https://www.coremeridian.xyz/mobile/login"
    case Logout = "https://www.coremeridian.xyz/mobile/logout"
}

class HTTPSServerAccess: NSObject {
    
    var credentials: Dictionary<String, String>!
    
    let timeout = NSTimeInterval(10)
    
    var url: NSURL!
    var method: HTTPMethod!
    var session: NSURLSession!
    var dataTask: NSURLSessionDataTask?
    var configuration: NSURLSessionConfiguration?
    
    init(path: HTTPSServerAccessPath, method: HTTPMethod, sessionType: HTTPSessionType, identifier: String?) {
        self.method = method
        url = NSURL(string: path.rawValue)
        
        switch path {
        case .Register, .Login:
            credentials = ["buffer" : "", "device_id" : String(user.hidden.deviceID), "password" : String(user.hidden.password), "location" : locationManager.getCoordinatesAsString()]
        default: credentials = ["token" : user.hidden.token as String]
        }
        
        switch sessionType {
        case .Default: configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        case .Ephemeral: configuration = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        case .Background:
            if let identity = identifier {
                configuration = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier(identity)
            }
            else {
                session = NSURLSession.sharedSession()
            }
        }
        session = NSURLSession(configuration: configuration!)
    }
    
    func connect() {
        let request = NSMutableURLRequest(URL: url!, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: timeout)
        
        request.HTTPMethod = method.rawValue
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(credentials, options: [])
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTaskWithRequest(request) { (data, response, error) in
            guard data != nil else {
                print("No data found: \(error)")
                return
            }
            do {
                if let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                    let success = json["success"] as? Int
                    let payload = ServicePayload(data: json)
                    payload.commit()
                    print("Success: \(success)")
                }
                else {
                    let json = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("Parse error: \(json)")
                }
            } catch let parseError {
                print(parseError)
                let json = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Parse error: \(json)")
            }
        }
        
        task.resume()
    }
    
    static func register() {
        let serverAccess = HTTPSServerAccess(path: HTTPSServerAccessPath.Register, method: HTTPMethod.POST, sessionType: HTTPSessionType.Default, identifier: nil)
        serverAccess.connect()
    }
    
    static func login() {
        let serverAccess = HTTPSServerAccess(path: HTTPSServerAccessPath.Login, method: HTTPMethod.POST, sessionType: HTTPSessionType.Default, identifier: nil)
        serverAccess.connect()
    }
    
    static func logout() {
        let serverAccess = HTTPSServerAccess(path: HTTPSServerAccessPath.Logout, method: HTTPMethod.POST, sessionType: HTTPSessionType.Default, identifier: nil)
        serverAccess.connect()
    }
}